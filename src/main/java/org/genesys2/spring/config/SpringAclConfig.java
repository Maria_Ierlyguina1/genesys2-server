/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.spring.config;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.genesys2.server.cache.HazelcastAclCache;
import org.genesys2.server.security.Slf4jAuditLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.acls.AclPermissionEvaluator;
import org.springframework.security.acls.domain.AclAuthorizationStrategyImpl;
import org.springframework.security.acls.domain.DefaultPermissionGrantingStrategy;
import org.springframework.security.acls.jdbc.BasicLookupStrategy;
import org.springframework.security.acls.jdbc.JdbcMutableAclService;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@Configuration
public class SpringAclConfig {

	@Autowired
	private DataSource dataSource;

	@Autowired
	private CacheManager cacheManager;

	@Bean(name = "permissionEvaluator")
	public AclPermissionEvaluator aclPermissionEvaluator() {
		return new AclPermissionEvaluator(aclService());
	}

	@Bean
	public JdbcMutableAclService aclService() {
		return new JdbcMutableAclService(dataSource, lookupStrategy(), aclCache());
	}

	@Bean
	public AclAuthorizationStrategyImpl aclAuthorizationStrategy() {
		return new AclAuthorizationStrategyImpl(new SimpleGrantedAuthority("ADMINISTRATOR"));
	}

	@Bean
	public Slf4jAuditLogger slf4jAuditLogger() {
		return new Slf4jAuditLogger();
	}

	@Bean
	public DefaultPermissionGrantingStrategy permissionGrantingStrategy() {
		return new DefaultPermissionGrantingStrategy(slf4jAuditLogger());
	}

	@Bean
	public HazelcastAclCache aclCache() {
		return new HazelcastAclCache(aclCacheCache(), permissionGrantingStrategy(), aclAuthorizationStrategy());
	}

	@Bean
	public Cache aclCacheCache() {
		return cacheManager.getCache("aclcache");
	}

	@Bean
	public BasicLookupStrategy lookupStrategy() {
		return new BasicLookupStrategy(dataSource, aclCache(), aclAuthorizationStrategy(), permissionGrantingStrategy());
	}

}
