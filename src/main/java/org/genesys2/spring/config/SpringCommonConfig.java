/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.spring.config;

import javax.persistence.Entity;

import org.apache.http.impl.client.HttpClientBuilder;
import org.genesys2.spring.validation.oval.spring.SpringOvalValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Scope;
import org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean;
import org.springframework.security.acls.AclPermissionEvaluator;
import org.springframework.security.web.access.expression.DefaultWebSecurityExpressionHandler;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Validator;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;

@EnableAspectJAutoProxy
@ComponentScan(basePackages = { "org.genesys2.spring", "org.genesys2.server" }, excludeFilters = {
		@ComponentScan.Filter(value = Controller.class, type = FilterType.ANNOTATION),
		@ComponentScan.Filter(value = Configuration.class, type = FilterType.ANNOTATION) }, includeFilters = @ComponentScan.Filter(value = Entity.class, type = FilterType.ANNOTATION))
@Configuration
public class SpringCommonConfig {

	@Autowired
	private AclPermissionEvaluator aclPermissionEvaluator;

	@Scope("singleton")
	@Bean
	public Validator validator() {
		final SpringOvalValidator validator = new SpringOvalValidator();
		validator.setValidator(new net.sf.oval.Validator());
		return validator;
	}

	@Bean
	public Jackson2ObjectMapperFactoryBean objectMapper() {
		final Jackson2ObjectMapperFactoryBean mapperFactoryBean = new Jackson2ObjectMapperFactoryBean();
		mapperFactoryBean.setFeaturesToDisable(SerializationFeature.FAIL_ON_EMPTY_BEANS, DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		return mapperFactoryBean;
	}

	@Bean
	public DefaultWebSecurityExpressionHandler webExpressionHandler() {
		final DefaultWebSecurityExpressionHandler handler = new DefaultWebSecurityExpressionHandler();
		handler.setPermissionEvaluator(aclPermissionEvaluator);
		return handler;
	}

	@Bean
	public HttpClientBuilder httpClientBuilder() {
		return HttpClientBuilder.create();
	}
}
