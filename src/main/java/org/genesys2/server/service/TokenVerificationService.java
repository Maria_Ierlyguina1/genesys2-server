/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service;

import org.genesys2.server.model.impl.VerificationToken;

public interface TokenVerificationService {

	/**
	 * Generate a new verification token (uuid+key) for specified tokenPurpose
	 *
	 * @param tokenPurpose
	 * @param data
	 * @return
	 */
	VerificationToken generateToken(String tokenPurpose, String data);

	/**
	 * Cancel (remove) token
	 *
	 * @param tokenUuid
	 * @throws NoSuchVerificationTokenException
	 */
	void cancel(String tokenUuid) throws NoSuchVerificationTokenException;

	/**
	 * Check token validity, remove it from persistence
	 *
	 * @param purpose
	 * @param tokenUuid
	 * @param key
	 * @return The consumed token
	 * @throws NoSuchVerificationTokenException
	 */
	VerificationToken consumeToken(String purpose, String tokenUuid, String key) throws NoSuchVerificationTokenException;

	public static class NoSuchVerificationTokenException extends Exception {

		/**
		 *
		 */
		private static final long serialVersionUID = -1127854381492707753L;
		
	}
}
