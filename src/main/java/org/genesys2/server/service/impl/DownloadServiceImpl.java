/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.common.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.genesys2.server.model.genesys.AccessionAlias;
import org.genesys2.server.model.genesys.AccessionBreeding;
import org.genesys2.server.model.genesys.AccessionCollect;
import org.genesys2.server.model.genesys.AccessionData;
import org.genesys2.server.model.genesys.AccessionExchange;
import org.genesys2.server.model.genesys.AccessionGeo;
import org.genesys2.server.model.genesys.AccessionId;
import org.genesys2.server.model.genesys.AccessionRemark;
import org.genesys2.server.model.genesys.PDCI;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepository;
import org.genesys2.server.persistence.domain.MethodRepository;
import org.genesys2.server.service.DownloadService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GenesysService.AllStuff;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DownloadServiceImpl implements DownloadService {

	public static final Log LOG = LogFactory.getLog(DownloadServiceImpl.class);

	@Autowired
	private EntityManager entityManager;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private GenesysLowlevelRepository genesysLowlevelRepository;

	@Autowired
	private MethodRepository methodRepository;

	@Autowired(required = false)
	private PDCICalculator pdciCalculator;

	@Value("${base.url}")
	private String baseUrl;

	private HashMap<String, Method> pdciMethods;

	@Override
	@Transactional(readOnly = true)
	public void writeXlsxMCPD(final AppliedFilters filters, final OutputStream outputStream) throws IOException {
		XSSFWorkbook template = new XSSFWorkbook(getClass().getResourceAsStream("/template/download/MCPD.xlsx"));

		// keep 1000 rows in memory, exceeding rows will be flushed to disk
		SXSSFWorkbook wb = new SXSSFWorkbook(template, -1);

		CellStyle dateStyle = wb.createCellStyle();
		dateStyle.setDataFormat(wb.createDataFormat().getFormat("dd/mm/yyyy"));
		dateStyle.setAlignment(CellStyle.ALIGN_LEFT);

		Sheet legal = wb.getSheet("Legal information");
		Row r;
		Cell c;
		r = legal.createRow(0);
		r.createCell(0).setCellValue("Server URL");
		r.createCell(1).setCellValue(baseUrl);
		r = legal.createRow(1);
		r.createCell(0).setCellValue("Filters");
		r.createCell(1).setCellValue(filters.toString());
		r = legal.createRow(2);
		r.createCell(0).setCellValue("Data source");
		c = r.createCell(1);
		c.setCellValue(baseUrl + "/explore?filter=" + filters.toString());

		r = legal.createRow(3);
		r.createCell(0).setCellValue("Date");
		c = r.createCell(1);
		c.setCellStyle(dateStyle);
		c.setCellValue(new Date());
		r = legal.createRow(4);
		r.createCell(0).setCellValue("Attribution");
		r.createCell(1).setCellValue(baseUrl + "/content/terms");
		((SXSSFSheet) legal).flushRows();
		outputStream.flush();

		final Sheet sheet = wb.getSheet("MCPD");

		final ArrayList<Long> batch = new ArrayList<Long>(100);
		final Writer writer = new Writer();

		// Write accession information
		genesysLowlevelRepository.listAccessionIds(filters, new Sort("acceNumb"), new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				long accessionId = rs.getLong(1);

				if (batch.size() == 500) {
					writer.processBatch(sheet, batch);
					batch.clear();
				}
				batch.add(accessionId);
			}
		});
		writer.processBatch(sheet, batch);

		((SXSSFSheet) sheet).flushRows();

		LOG.info("Writing to output stream");
		wb.write(outputStream);
		wb.dispose();
		outputStream.flush();
		LOG.info("Done");
	}

	private class Writer {
		int i = 0;

		private void processBatch(Sheet sheet, List<Long> batch) {
			List<AllStuff> alls = genesysService.loadAllStuff(batch);

			for (AllStuff all : alls) {
				Row row = sheet.createRow(i + 1);
				writeMCPDRow(sheet, row, all.accession, all.geo, all.collect, all.bred, all.names, all.exch, all.remarks);
				i++;
			}

			LOG.info("Writing XLSX row " + i);
			try {
				((SXSSFSheet) sheet).flushRows();
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
			}
		}

		private void processPDCIBatch(Sheet sheet, List<Long> batch) {
			List<PDCI> alls = genesysService.loadPDCI(batch);

			for (PDCI pdci : alls) {
				Row row = sheet.createRow(i + 1);
				writePDCIRow(sheet, row, pdci);
				i++;
			}

			LOG.info("Writing XLSX row " + i);
			try {
				((SXSSFSheet) sheet).flushRows();
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
			}
		}
	}

	private void writeMCPDRow(Sheet sheet, Row row, AccessionData accession, AccessionGeo geo, AccessionCollect collect, AccessionBreeding bred,
			List<AccessionAlias> names, AccessionExchange exch, List<AccessionRemark> remarks) {

		// Process and write result
		row.createCell(0).setCellValue(accession.getInstitute().getCode());
		row.createCell(1).setCellValue(accession.getAccessionName());

		if (collect != null) {
			createCell(row, 2, collect.getCollNumb());
			createCell(row, 3, collect.getCollCode());
			createCell(row, 4, collect.getCollName());
			createCell(row, 5, collect.getCollInstAddress());
			createCell(row, 6, collect.getCollMissId());
			createCell(row, 16, collect.getCollSite());
			createCell(row, 23, collect.getCollDate());
		}

		if (bred != null) {
			createCell(row, 24, bred.getBreederCode());
			createCell(row, 27, bred.getPedigree());
		}

		Taxonomy2 taxonomy = accession.getTaxonomy();
		if (taxonomy != null) {
			createCell(row, 7, taxonomy.getGenus());
			createCell(row, 8, taxonomy.getSpecies());
			createCell(row, 9, taxonomy.getSpAuthor());
			createCell(row, 10, taxonomy.getSubtaxa());
			createCell(row, 11, taxonomy.getSubtAuthor());
		}

		createCell(row, 14, accession.getAcquisitionDate());

		Country origin = accession.getCountryOfOrigin();
		if (origin != null) {
			createCell(row, 15, origin.getCode3());
		}

		if (geo != null) {
			createCell(row, 17, geo.getLatitude());
			createCell(row, 18, geo.getLongitude());
			createCell(row, 19, geo.getUncertainty());
			createCell(row, 20, geo.getDatum());
			createCell(row, 21, geo.getMethod());
			createCell(row, 22, geo.getElevation());

		}

		createCell(row, 26, accession.getSampleStatus());

		if (StringUtils.isNotBlank(accession.getAcquisitionSource())) {
			Cell c = row.createCell(28);
			try {
				c.setCellValue(Integer.parseInt(accession.getAcquisitionSource()));
			} catch (NumberFormatException e) {
				c.setCellValue(accession.getAcquisitionSource());
			}
		}

		createCell(row, 33, accession.getDuplSite());
		createCell(row, 35, accession.getStorage());
		createCell(row, 36, accession.getMlsStatus());

		if (names != null && names.size() > 0) {
			String acceName = null;
			String otherNumb = null;
			String donorNumb = null;
			for (AccessionAlias a : names) {
				switch (a.getAliasType()) {
				case ACCENAME:
					acceName = addName(acceName, a.getName());
					break;
				case DONORNUMB:
					donorNumb = a.getName();
					break;
				case OTHERNUMB:
					otherNumb = addName(otherNumb, a.getName(), a.getUsedBy());
					break;
				default:
					break;
				}
			}

			createCell(row, 13, acceName);
			createCell(row, 31, donorNumb);
			createCell(row, 32, otherNumb);
		}

		if (exch != null) {
			createCell(row, 29, exch.getDonorInstitute());
			createCell(row, 30, exch.getDonorName());
			// row.createCell(32).setCellValue(exch.getAccNumbDonor());
		}

		if (accession.getUuid() != null) {
			XSSFHyperlink hypr = (XSSFHyperlink) sheet.getWorkbook().getCreationHelper().createHyperlink(Hyperlink.LINK_URL);
			hypr.setAddress("http://purl.org/germplasm/id/" + accession.getUuid());
			Cell cell = createCell(row, 37, accession.getUuid().toString());
			cell.setHyperlink(hypr);
		}
		if (accession.getHistoric()) {
			createCell(row, 38, accession.getHistoric());
		}

		if (accession.getAcceUrl() != null) {
			XSSFHyperlink hypr = (XSSFHyperlink) sheet.getWorkbook().getCreationHelper().createHyperlink(Hyperlink.LINK_URL);
			hypr.setAddress(accession.getAcceUrl());
			Cell cell = createCell(row, 39, accession.getAcceUrl());
			cell.setHyperlink(hypr);
		}

		if (remarks != null && remarks.size() > 0) {
			String r = "";
			for (AccessionRemark remark : remarks) {
				if (r.length() > 0)
					r += ";";
				if (StringUtils.isNotBlank(remark.getFieldName())) {
					r += remark.getFieldName() + ":" + remark.getRemark();
				} else {
					r += remark.getRemark();
				}
			}

			createCell(row, 40, r);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public void writeXlsxPDCI(final AppliedFilters filters, final OutputStream outputStream) throws IOException {
		XSSFWorkbook template = new XSSFWorkbook(getClass().getResourceAsStream("/template/download/PDCI.xlsx"));

		// keep 1000 rows in memory, exceeding rows will be flushed to disk
		SXSSFWorkbook wb = new SXSSFWorkbook(template, -1);

		CellStyle dateStyle = wb.createCellStyle();
		dateStyle.setDataFormat(wb.createDataFormat().getFormat("dd/mm/yyyy"));
		dateStyle.setAlignment(CellStyle.ALIGN_LEFT);

		Sheet legal = wb.getSheet("Legal information");
		Row r;
		Cell c;
		r = legal.createRow(0);
		r.createCell(0).setCellValue("Server URL");
		r.createCell(1).setCellValue(baseUrl);
		r = legal.createRow(1);
		r.createCell(0).setCellValue("Filters");
		r.createCell(1).setCellValue(filters.toString());
		r = legal.createRow(2);
		r.createCell(0).setCellValue("Data source");
		c = r.createCell(1);
		c.setCellValue(baseUrl + "/explore?filter=" + filters.toString());

		r = legal.createRow(3);
		r.createCell(0).setCellValue("Date");
		c = r.createCell(1);
		c.setCellStyle(dateStyle);
		c.setCellValue(new Date());
		r = legal.createRow(4);
		r.createCell(0).setCellValue("Attribution");
		r.createCell(1).setCellValue(baseUrl + "/content/terms");
		((SXSSFSheet) legal).flushRows();
		outputStream.flush();

		final Sheet sheet = wb.getSheet("PDCI");

		final ArrayList<Long> batch = new ArrayList<Long>(100);
		final Writer writer = new Writer();

		// Write accession information
		genesysLowlevelRepository.listAccessionIds(filters, new Sort("acceNumb"), new RowCallbackHandler() {
			@Override
			public void processRow(ResultSet rs) throws SQLException {
				long accessionId = rs.getLong(1);

				if (batch.size() == 500) {
					writer.processPDCIBatch(sheet, batch);
					batch.clear();
				}
				batch.add(accessionId);
			}
		});
		writer.processPDCIBatch(sheet, batch);

		((SXSSFSheet) sheet).flushRows();

		LOG.info("Writing to output stream");
		wb.write(outputStream);
		wb.dispose();
		outputStream.flush();
		LOG.info("Done");
	}

	private void writePDCIRow(Sheet sheet, Row row, PDCI pdci) {
		// Process and write result
		AccessionId accession = pdci.getAccession();
		if (accession.getUuid() != null) {
			XSSFHyperlink hypr = (XSSFHyperlink) sheet.getWorkbook().getCreationHelper().createHyperlink(Hyperlink.LINK_URL);
			hypr.setAddress("http://purl.org/germplasm/id/" + accession.getUuid());
			Cell cell = createCell(row, 0, accession.getUuid().toString());
			cell.setHyperlink(hypr);
		}
		createCell(row, 1, pdci.getScore());
		int col = 2;
		for (String indicator : pdci.getIndependentItems()) {
			createCell(row, col++, getPDCIScore(pdci, indicator));
		}
		for (String indicator : pdci.getDependentItems()) {
			createCell(row, col++, getPDCIScore(pdci, indicator));
		}
	}

	private Number getPDCIScore(PDCI pdci, String indicator) {
		synchronized (this) {
			if (pdciMethods == null) {
				pdciMethods = new HashMap<String, Method>();
				try {
					for (String name : PDCI.independentItems) {
						Method m = PDCI.class.getMethod("get" + StringUtils.capitalize(name));
						if (m.getReturnType() == int.class) {
							pdciMethods.put(name, m);
						}
					}
					for (String name : PDCI.dependentItems) {
						Method m = PDCI.class.getMethod("get" + StringUtils.capitalize(name));
						if (m.getReturnType() == int.class) {
							pdciMethods.put(name, m);
						}
					}
				} catch (Throwable e) {
					LOG.warn(e.getMessage(), e);
				}
			}
		}
		try {
			Method m = pdciMethods.get(indicator);
			if (m != null) {
				return (int) m.invoke(pdci);
			} else {
				return -1;
			}
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			LOG.warn(e.getMessage());
			return -1;
		}
	}

	private Cell createCell(Row row, int column, Number integer) {
		if (integer == null)
			return null;
		Cell c = row.createCell(column);
		c.setCellValue(integer.doubleValue());
		return c;
	}

	private Cell createCell(Row row, int column, Boolean bool) {
		if (bool == null)
			return null;
		Cell c = row.createCell(column);
		c.setCellValue(bool);
		return c;
	}

	private Cell createCell(Row row, int column, String value) {
		if (StringUtils.isBlank(value))
			return null;
		Cell c = row.createCell(column);
		c.setCellValue(value);
		return c;
	}

	private String addName(String otherNames, String name, String usedBy) {
		if (StringUtils.isBlank(name)) {
			return otherNames;
		} else if (StringUtils.isBlank(usedBy)) {
			return addName(otherNames, name);
		} else {
			return addName(otherNames, usedBy + ":" + name);
		}
	}

	private String addName(String otherNames, String name) {
		if (StringUtils.isBlank(name))
			return otherNames;
		if (otherNames == null)
			return name;
		return otherNames + ";" + name;
	}
}
