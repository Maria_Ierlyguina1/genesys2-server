/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.elastic.AccessionDetails;
import org.genesys2.server.model.elastic.Alias;
import org.genesys2.server.model.elastic.Collect;
import org.genesys2.server.model.elastic.Geo;
import org.genesys2.server.model.elastic.Taxonomy;
import org.genesys2.server.model.genesys.AccessionAlias.AliasType;
import org.genesys2.server.model.genesys.PDCI;
import org.genesys2.server.service.GenesysService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Calculates PDCI value for an accession.
 * 
 * <p>
 * Theo van Hintum, Frank Menting and Elisabeth van Strien (2011). <b>Quality
 * indicators for passport data in ex situ genebanks.</b> Plant Genetic
 * Resources, 9, pp 478-485.
 * 
 * doi:10.1017/S1479262111000682
 * </p>
 */
@Component
public class PDCICalculator implements InitializingBean {
	public static final Log LOG = LogFactory.getLog(PDCICalculator.class);

	@Autowired
	private GenesysService genesysService;

	@Override
	public void afterPropertiesSet() throws Exception {
	}

	public PDCI makePDCI(long accessionId) {
		return calculatePDCI(new PDCI(), accessionId);
	}

	public PDCI updatePdci(PDCI pdci, long accessionId) {
		return calculatePDCI(pdci, accessionId);
	}

	private PDCI calculatePDCI(PDCI pdci, long accessionId) {
		AccessionDetails accessionDetails = genesysService.getAccessionDetails(accessionId);
		if (accessionDetails == null) {
			LOG.warn("No such accession " + accessionId);
			return null;
		}

		return updatePdci(pdci, accessionDetails);
	}

	public PDCI updatePdci(PDCI pdci, AccessionDetails accessionDetails) {
		pdci.reset();

		independentDescriptors(pdci, accessionDetails);

		switch (accessionDetails.getSampStat() == null ? -1 : accessionDetails.getSampStat() / 100) {
		case 1:
		case 2:
			wildOrWeedy(pdci, accessionDetails);
			break;

		case 3:
			landrace(pdci, accessionDetails);
			break;
		case 4:
			breedingMaterial(pdci, accessionDetails);
			break;
		case 5:
			cultivar(pdci, accessionDetails);
		default:
			otherTypes(pdci, accessionDetails);
		}

		return pdci;
	}

	private void independentDescriptors(PDCI pdci, AccessionDetails accessionDetails) {

		taxonomic(pdci, accessionDetails.getTaxonomy());

		if (accessionDetails.getCrops() != null && accessionDetails.getCrops().size() > 0) {
			pdci.setCropName(45);
		}

		if (StringUtils.isNotBlank(accessionDetails.getAcqDate())) {
			pdci.setAcqDate(10);
		}
		if (accessionDetails.getSampStat() != null) {
			pdci.setSampStat(80);
		}
		if (StringUtils.isNotBlank(accessionDetails.getDonorCode())) {
			pdci.setDonorCode(40);
		}
		if (StringUtils.isNotBlank(accessionDetails.getDonorNumb())) {
			if (StringUtils.isBlank(accessionDetails.getDonorCode()) && StringUtils.isBlank(accessionDetails.getDonorName())) {
				pdci.setDonorNumb(30);
			} else {
				pdci.setDonorNumb(40);
			}
		}

		if (names(accessionDetails.getAliases(), AliasType.OTHERNUMB)) {
			pdci.setOtherNumb(35);
		}

		if (accessionDetails.getDuplSite() != null && accessionDetails.getDuplSite().size() > 0) {
			pdci.setDuplSite(30);
		} else {
			// Missing DUPLINSTNAME in Genesys, would add 15
			pdci.setDuplInstName(0);
		}

		if (accessionDetails.getStorage() != null && accessionDetails.getStorage().size() > 0) {
			pdci.setStorage(15);
		}

		if (StringUtils.isNotBlank(accessionDetails.getDonorName())) {
			if (StringUtils.isBlank(accessionDetails.getDonorCode())) {
				pdci.setDonorName(20);
			} else {
				pdci.setDonorName(0);
			}
		}

		if (accessionDetails.getAcceUrl() != null) {
			// pdci.setAcceUrl(40);
		}

		if (accessionDetails.getMlsStatus() != null) {
			pdci.setMlsStat(15);
		}
	}

	private void taxonomic(PDCI pdci, Taxonomy taxonomy) {
		if (taxonomy != null) {
			if (!StringUtils.isBlank(taxonomy.getGenus())) {
				pdci.setGenus(120);
				if (!StringUtils.isBlank(taxonomy.getSpecies()) && !StringUtils.equalsIgnoreCase("sp.", taxonomy.getSpecies())) {
					pdci.setSpecies(80);

					if (!StringUtils.isBlank(taxonomy.getSpAuthor())) {
						pdci.setSpAuthor(5);
					}
					if (!StringUtils.isBlank(taxonomy.getSubtaxa())) {
						pdci.setSubTaxa(40);

						if (!StringUtils.isBlank(taxonomy.getSubtAuthor())) {
							pdci.setSubtAuthor(5);
						}
					}
				}
			}
		}
	}

	private void wildOrWeedy(PDCI pdci, AccessionDetails accessionDetails) {
		Geo geo = accessionDetails.getGeo();
		Collect coll = accessionDetails.getColl();

		if (accessionDetails.getOrgCty() != null) {
			pdci.setOrigCty(80);
		}

		boolean hasLatLng = false;

		if (geo != null) {
			if (geo.getLatitude() != null && geo.getLongitude() != null) {
				if (geo.getLatitude() < -90 || geo.getLatitude() > 90 || geo.getLongitude() < -180 || geo.getLongitude() > 180) {
					// Ignore coordinates
				} else {
					pdci.setLatitude(60);
					pdci.setLongitude(60);
					hasLatLng = true;
				}
			}

			if (geo.getElevation() != null) {
				pdci.setElevation(20);
			}
		}

		if (coll != null) {
			if (StringUtils.isNotBlank(coll.collSite)) {
				if (hasLatLng)
					pdci.setCollSite(20);
				else
					pdci.setCollSite(70);
			}

			if (StringUtils.isNotBlank(coll.collDate)) {
				pdci.setCollDate(30);
			}
			if (coll.collSrc != null) {
				pdci.setCollSrc(30);
			}

			if (StringUtils.isNotBlank(coll.collNumb)) {
				pdci.setCollNumb(60);
			}
			if (coll.collCode != null && coll.collCode.size() > 0) {
				pdci.setCollCode(40);
			} else {
				if (StringUtils.isNotBlank(coll.collName)) {
					pdci.setCollName(20);
				}
			}
		}
	}

	private void landrace(PDCI pdci, AccessionDetails accessionDetails) {
		Geo geo = accessionDetails.getGeo();
		Collect coll = accessionDetails.getColl();

		if (accessionDetails.getOrgCty() != null) {
			pdci.setOrigCty(80);
		}

		boolean hasLatLng = false;

		if (geo != null) {
			if (geo.getLatitude() != null && geo.getLongitude() != null) {
				pdci.setLatitude(40);
				pdci.setLongitude(40);
				hasLatLng = true;
			}

			if (geo.getElevation() != null) {
				pdci.setElevation(15);
			}
		}

		if (coll != null) {
			if (StringUtils.isNotBlank(coll.collSite)) {
				if (hasLatLng)
					pdci.setCollSite(15);
				else
					pdci.setCollSite(45);
			}

			if (StringUtils.isNotBlank(coll.collDate)) {
				pdci.setCollDate(30);
			}

			if (StringUtils.isNotBlank(accessionDetails.getPedigree())) {
				pdci.setAncest(10);
			}

			if (coll.collSrc != null) {
				pdci.setCollSrc(50);
			}

			if (names(accessionDetails.getAliases(), AliasType.ACCENAME)) {
				pdci.setAcceName(50);
			}

			if (StringUtils.isNotBlank(coll.collNumb)) {
				pdci.setCollNumb(40);
			}
			if (coll.collCode != null && coll.collCode.size() > 0) {
				pdci.setCollCode(30);
			} else {
				if (StringUtils.isNotBlank(coll.collName)) {
					pdci.setCollName(15);
				}
			}
		}
	}

	private void breedingMaterial(PDCI pdci, AccessionDetails accessionDetails) {
		Collect coll = accessionDetails.getColl();

		if (accessionDetails.getOrgCty() != null) {
			pdci.setOrigCty(40);
		}

		if (StringUtils.isNotBlank(accessionDetails.getBredCode())) {
			pdci.setBredCode(110);
		}

		if (StringUtils.isNotBlank(accessionDetails.getPedigree())) {
			pdci.setAncest(150);
		}

		if (coll != null) {
			if (StringUtils.isNotBlank(coll.collSite)) {
			}

			if (StringUtils.isNotBlank(coll.collDate)) {
			}

			if (coll.collSrc != null) {
				pdci.setCollSrc(20);
			}

			if (names(accessionDetails.getAliases(), AliasType.ACCENAME)) {
				pdci.setAcceName(80);
			}

			if (StringUtils.isNotBlank(coll.collNumb)) {
			}

			if (coll.collCode != null && coll.collCode.size() > 0) {
			}
		}
	}

	private void cultivar(PDCI pdci, AccessionDetails accessionDetails) {
		Collect coll = accessionDetails.getColl();

		if (accessionDetails.getOrgCty() != null) {
			pdci.setOrigCty(40);
		}

		if (StringUtils.isNotBlank(accessionDetails.getBredCode())) {
			pdci.setBredCode(80);
		}

		if (StringUtils.isNotBlank(accessionDetails.getPedigree())) {
			pdci.setAncest(100);
		}

		if (coll != null) {
			if (StringUtils.isNotBlank(coll.collSite)) {
			}

			if (StringUtils.isNotBlank(coll.collDate)) {
			}

			if (coll.collSrc != null) {
				pdci.setCollSrc(20);
			}

			if (names(accessionDetails.getAliases(), AliasType.ACCENAME)) {
				pdci.setAcceName(160);
			}

			if (StringUtils.isNotBlank(coll.collNumb)) {
			}

			if (coll.collCode != null && coll.collCode.size() > 0) {
			}
		}
	}

	private void otherTypes(PDCI pdci, AccessionDetails accessionDetails) {
		Geo geo = accessionDetails.getGeo();
		Collect coll = accessionDetails.getColl();

		if (accessionDetails.getOrgCty() != null) {
			pdci.setOrigCty(40);
		}

		boolean hasLatLng = false;

		if (geo != null) {
			if (geo.getLatitude() != null && geo.getLongitude() != null) {
				pdci.setLatitude(15);
				pdci.setLongitude(15);
				hasLatLng = true;
			}

			if (geo.getElevation() != null) {
				pdci.setElevation(5);
			}
		}

		if (StringUtils.isNotBlank(accessionDetails.getBredCode())) {
			pdci.setBredCode(10);
		}

		if (StringUtils.isNotBlank(accessionDetails.getPedigree())) {
			pdci.setBredCode(40);
		}

		if (names(accessionDetails.getAliases(), AliasType.ACCENAME)) {
			pdci.setAcceName(40);
		}

		if (coll != null) {
			if (StringUtils.isNotBlank(coll.collSite)) {
				if (hasLatLng)
					pdci.setCollSite(10);
				else
					pdci.setCollSite(20);
			}

			if (StringUtils.isNotBlank(coll.collDate)) {
				pdci.setCollDate(10);
			}

			if (coll.collSrc != null) {
				pdci.setCollSrc(25);
			}

			if (StringUtils.isNotBlank(coll.collNumb)) {
				pdci.setCollNumb(20);
			}
			if (coll.collCode != null && coll.collCode.size() > 0) {
				pdci.setCollCode(20);
			} else {
				if (StringUtils.isNotBlank(coll.collName)) {
					pdci.setCollName(10);
				}
			}
		}
	}

	private boolean names(ArrayList<Alias> aliases, AliasType type) {
		if (aliases == null || aliases.size() == 0)
			return false;

		for (Alias alias : aliases) {
			if (alias.getType() == type) {
				return true;
			}
		}
		return false;
	}

}
