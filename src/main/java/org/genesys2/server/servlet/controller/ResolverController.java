/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.util.List;
import java.util.UUID;

import org.genesys2.server.model.IdUUID;
import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.AccessionData;
import org.genesys2.server.model.genesys.AccessionHistoric;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.ResolverService;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/resolver")
public class ResolverController {

	@Autowired
	private ResolverService resolverService;

	@Autowired
	private InstituteService instituteService;

	@RequestMapping(value = "")
	public String form() {
		return "/resolver/index";
	}

	@RequestMapping(value = "/reverse", method = RequestMethod.POST, params = { "acceNumb" })
	public String reverse(ModelMap model, @RequestParam(required = true, value = "acceNumb") String acceNumb,
			@RequestParam(required = false, value = "instCode") String instCode) {

		FaoInstitute faoInstitute = instituteService.findInstitute(instCode);
		List<AccessionData> accessions = resolverService.findMatches(faoInstitute, acceNumb);
		model.addAttribute("accessions", accessions);
		return "/resolver/reverse";
	}

	@RequestMapping(value = "/forward", method = RequestMethod.POST, params = { "uuid" })
	public String forwardGui(ModelMap model, @RequestParam(required = true, value = "uuid") UUID uuid) {
		return forwardUuid(model, uuid);
	}

	@RequestMapping(value = "/{uuid:.{36}}", method = RequestMethod.GET)
	public String forwardUuid(ModelMap model, @PathVariable(value = "uuid") UUID uuid) {

		final IdUUID resolved = resolverService.forward(uuid);

		if (resolved == null) {
			throw new ResourceNotFoundException();
		}

		if (resolved instanceof Accession) {
			return "redirect:/acn/id/" + ((Accession) resolved).getId();
		}

		if (resolved instanceof AccessionHistoric) {
			return "redirect:/archive/" + resolved.getUuid();
		}

		throw new ResourceNotFoundException("We don't have an object with UUID " + uuid);
	}
}
