/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rest;

import java.util.List;

import net.sf.oval.ConstraintViolation;
import net.sf.oval.Validator;
import net.sf.oval.constraint.MinLength;
import net.sf.oval.constraint.NotBlank;
import net.sf.oval.constraint.NotNull;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.ServiceEndpoints;
import org.genesys2.server.exception.AuthorizationException;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.TeamService;
import org.genesys2.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@PreAuthorize("isAuthenticated()")
@RequestMapping(value = { "/api/v0", "/json/v0" })
public class UserController extends RestController {
	private static final Log LOG = LogFactory.getLog(UserController.class);

	@Autowired
	protected UserService userService;

	@Autowired
	protected TeamService teamService;

	@Autowired
	protected InstituteService instituteService;

	/**
	 * Returns logged in user's profile
	 *
	 * @return - user's profile
	 */
	@RequestMapping(value = ServiceEndpoints.ME, method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	Object getProfile() throws AuthorizationException {
		final User user = userService.getMe();
		return OAuth2Cleanup.clean(user);
	}

	@RequestMapping(value = "/me/institutes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	Object listInstitutes() {
		return instituteService.listMyInstitutes(new Sort("code"));
	}

	@RequestMapping(value = "/me/teams", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	Object listTeams() {
		return teamService.listMyTeams();
	}

	@RequestMapping(value = "/me/teams", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('VETTEDUSER')")
	public @ResponseBody
	Object createTeam(@RequestBody TeamJson teamJson) {
		final Validator validator = new Validator();
		final List<ConstraintViolation> violations = validator.validate(teamJson);
		if (violations.size() > 0) {
			// TODO We could do better messages on validation error
			throw new ModelValidationException("Validation error", violations);
		}
		LOG.info("Creating team " + teamJson);
		return OAuth2Cleanup.clean(teamService.addTeam(teamJson.name));
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping(value = "/user/{uuid}/locked", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	boolean changeLock(@PathVariable("uuid") String uuid, @RequestBody boolean locked) {
		userService.setAccountLock(uuid, locked);
		return true;
	}

	@PreAuthorize("hasRole('ADMINISTRATOR')")
	@RequestMapping(value = "/user/{uuid}/enabled", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	boolean changeEnabled(@PathVariable("uuid") String uuid, @RequestBody boolean enabled) {
		userService.setAccountEnabled(uuid, enabled);
		return true;
	}

	@RequestMapping(value = "/me/teams/{teamId}/leave", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	Object leaveTeam(@PathVariable("teamId") Long teamId) {
		LOG.info("Leaving team " + teamId);
		teamService.removeMe(teamId);
		return JSON_OK;
	}

	public static class TeamJson {
		@NotBlank
		@NotNull
		@MinLength(value = 4)
		public String name;
	}
}
