/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.rdf;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import org.genesys2.rdf.model.dc.DublinCore;
import org.genesys2.rdf.model.dwc.DarwinCore;
import org.genesys2.rdf.model.skos.SKOS;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.VersionedAuditedModel;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.service.UserService;
import org.genesys2.server.servlet.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.DC_11;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

/**
 * Controller which simply handles RDF requests
 */
public abstract class RdfBaseController extends BaseController {

	@Autowired
	private UserService userService;

	/**
	 * A String equivalent of {@link RdfBaseController#RDF_MEDIATYPE_XML}.
	 */
	public final static String RDF_MEDIATYPE_XML = "application/rdf+xml";

	/**
	 * A String equivalent of {@link RdfBaseController#RDF_MEDIATYPE_TURTLE}.
	 */
	public final static String RDF_MEDIATYPE_TURTLE = "text/turtle";

	protected static final String GCDT = "Global Crop Diversity Trust";
	protected static final String RDF_MODEL_VERSION = "Revision: 0.1";

	@Value("${base.url}")
	private String baseUrl;

	protected String getBaseUri() {
		if (baseUrl == null) {
			throw new NullPointerException("Base URI not set in RdfBaseController?");
		}
		return baseUrl;
	}

	protected static String modelVersion() {
		return RDF_MODEL_VERSION;
	}

	/*
	 * Method to set up the RDF model with basic global annotation.
	 */
	protected Model startModel() {
		final Model model = ModelFactory.createDefaultModel();
		model.setNsPrefix("rdf", RDF.getURI());
		model.setNsPrefix("rdfs", RDFS.getURI());
		model.setNsPrefix("dc", DC_11.getURI());
		model.setNsPrefix("dcterms", DublinCore.getURI());
		model.setNsPrefix("dwc", DarwinCore.getURI());
		model.setNsPrefix("skos", SKOS.getURI());
		model.setNsPrefix("genesys", getBaseUri() + "/");
		return model;
	}

	/**
	 * Generates a canonical RDF identifier from its input string (blanks
	 * removed, with title case or camel case words, depending on
	 * capitalizeFirst being true or false, respectively)
	 *
	 * @param identifier
	 * @param capitlizeFirst
	 *            - if true, then the first letter of the identifier is
	 *            capitalized
	 * @return
	 */
	public static String canonicalID(String identifier, Boolean capitalizeFirst) {
		final String[] words = identifier.split("\\s");
		identifier = "";
		for (final String word : words) {
			if (!capitalizeFirst) {
				// only run once in the loop if false (reset to true...)
				identifier += word.toLowerCase();
				capitalizeFirst = true;
			} else {
				identifier += word.substring(0, 1).toUpperCase();
				identifier += word.substring(1).toLowerCase();
			}
		}
		return identifier;
	}

	/*
	 * Method to create a subject Resource initialized with common core
	 * annotation.
	 */

	// Same as following method but defaults to SKOS.Concept ...
	private Resource createResource(Model model, VersionedAuditedModel entity, String uri, String tag, Boolean isProperty) {
		return createResource(model, entity, uri, tag, isProperty, false);
	}

	// This version checks a flag for SKOS:Collection status
	private Resource createResource(Model model, VersionedAuditedModel entity, String uri, String tag, Boolean isProperty, Boolean isCollection // SKOS
																																				// type
																																				// -
																																				// true
																																				// if
																																				// isa
																																				// SKOS:Collection
	) {

		final Resource subject = model.createResource(getBaseUri() + uri);

		subject.addProperty(DC_11.publisher, GCDT);
		if (isCollection) {
			subject.addProperty(RDF.type, SKOS.Collection);
		} else {
			subject.addProperty(RDF.type, SKOS.Concept);
		}

		if (isProperty) {
			tag = canonicalID(tag, false);
		} else {
			tag = canonicalID(tag, true);
		}
		subject.addProperty(SKOS.hiddenLabel, tag);

		// dc:creator <user>
		final Long userId = entity.getCreatedBy();
		if (userId != null) {
			try {
				final User user = userService.getUserById(userId);

				// just put the user's name for now?
				subject.addProperty(DC_11.creator, user.getName());

			} catch (final UserException e) {
				this._logger.debug("Invalid user access?", e);
			}
		}

		// dc:date <createdData>
		final Date createdDate = entity.getCreatedDate();
		if (createdDate != null) {
			subject.addProperty(DC_11.date, createdDate.toString());
		}

		// dc:hasVersion <createdData>
		final String version = String.valueOf(entity.getVersion());
		subject.addProperty(DublinCore.hasVersion, version);

		return subject;
	}

	/**
	 * Method to create a Subject Resource initialized with common core
	 * annotation.
	 */
	protected Resource createSubject(Model model, VersionedAuditedModel entity, String uri, String tag) {
		return createResource(model, entity, uri, tag, false);

	}

	/**
	 * Method to create a Property Resource initialized with common core
	 * annotation.
	 */
	protected Resource createProperty(Model model, VersionedAuditedModel entity, String uri, String tag) {
		return createResource(model, entity, uri, tag, true);

	}

	/**
	 * Method to create a Property Resource initialized with common core
	 * annotation. This version checks if the Resource is meant to be a SKOS
	 * Collection
	 */
	protected Resource createProperty(Model model, VersionedAuditedModel entity, String uri, String tag, Boolean isCollection) {
		return createResource(model, entity, uri, tag, true, isCollection);

	}

	/*
	 * Method to create an Object Resource initialized with common core
	 * annotation.
	 */
	protected Resource createObject(Model model, VersionedAuditedModel entity, String uri, String tag) {
		return createResource(model, entity, uri, tag, false);

	}

	protected void seeAlso(Model model, Resource subject, String seeAlso) {
		if (seeAlso != null) {
			subject.addProperty(RDFS.seeAlso, model.createResource(seeAlso));
		}
	}

	/**
	 * Method to write out the model as a Turtle RDF string
	 *
	 * @param model
	 *            - Apache Jena RDF Model class object of current model being
	 *            rendered
	 * @param format
	 *            Predefined values are "RDF/XML", "RDF/XML-ABBREV", "N-TRIPLE",
	 *            "TURTLE", (and "TTL") and "N3".
	 * @return
	 */
	/*
	 * TODO: maybe convert this to an HTTP Converter (which can handle diverse
	 * RDF formats?)
	 */
	protected String endModel(Model model, String format) {

		final ByteArrayOutputStream os = new ByteArrayOutputStream();
		final PrintStream ps = new PrintStream(os);

		if (format == null) {
			model.write(ps, "TURTLE");
		} else {
			model.write(ps, format);
		}

		String output = "";
		try {
			output = os.toString("UTF8");
		} catch (final UnsupportedEncodingException e) {
			_logger.debug("os.toString(\"UTF8\") failed while to write out parameter categories as Turtle RDF");
		}

		_logger.trace("RdfBaseController.endModel() returning RDF text: " + output);

		return output;
	}

	/*
	 * Method to write out the model as a Turtle RDF string (TODO: maybe convert
	 * this to an HTTP Converter which can handle other RDF formats?)
	 */
	protected String endModel(Model model) {
		return endModel(model, null);
	}
}
