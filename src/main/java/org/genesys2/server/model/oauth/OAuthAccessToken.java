/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.oauth;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.genesys2.server.model.BusinessModel;

@Entity
@Table(name = "oauthaccesstoken")
public class OAuthAccessToken extends BusinessModel implements OAuthToken {

	private static final long serialVersionUID = -424404058531521676L;

	@Column(nullable = false)
	private String clientId;

	@Column(length = 36)
	private String refreshToken;

	@Column
	private Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date expiration;

	@Column(length = 100)
	private String tokenType;

	@Column(length = 100, nullable = false)
	private String value;

	@Column(length = 100)
	private String scopes;

	@Column(length = 500)
	private String additionalInfo;

	@Column(length = 64)
	private String userUuid;

	@Column(length = 200)
	private String redirectUri;

	@Column(length = 200, nullable = false, unique = true)
	private String authenticationId;

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getAuthenticationId() {
		return authenticationId;
	}

	public void setAuthenticationId(String authenticationId) {
		this.authenticationId = authenticationId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}

	public Date getExpiration() {
		return expiration;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setScopes(String scopes) {
		this.scopes = scopes;
	}

	public String getScopes() {
		return scopes;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setUserUuid(String uuid) {
		this.userUuid = uuid;
	}

	public String getUserUuid() {
		return userUuid;
	}

	public String getRedirectUri() {
		return redirectUri;
	}

	public void setRedirectUri(String redirectUri) {
		this.redirectUri = redirectUri;
	}
}
