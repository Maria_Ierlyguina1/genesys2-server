package org.genesys2.server.model.elastic;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.genesys2.server.model.genesys.AccessionAlias;
import org.genesys2.server.model.genesys.AccessionBreeding;
import org.genesys2.server.model.genesys.AccessionCollect;
import org.genesys2.server.model.genesys.AccessionData;
import org.genesys2.server.model.genesys.AccessionExchange;
import org.genesys2.server.model.genesys.AccessionGeo;
import org.genesys2.server.model.genesys.AccessionRemark;
import org.genesys2.server.model.genesys.SvalbardData;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.Organization;
import org.genesys2.util.MCPDUtil;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "genesys", type = "mcpd", refreshInterval = "60s")
public class AccessionDetails {
	private static final Logger LOG = Logger.getLogger(AccessionDetails.class);

	@Version
	private Long version;
	@Id
	private Long id;

	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private UUID uuid;
	@Field(type = FieldType.String)
	private String acceNumb;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String acqDate;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String acqSrc;
	private Boolean available;
	private Date createdDate;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private List<String> duplSite;
	private Boolean mlsStatus;
	private Boolean art15;
	private Integer sampStat;
	private List<Integer> storage;

	@Field(type = FieldType.Object)
	private Institute institute;
	@Field(type = FieldType.Object)
	private Country orgCty;
	@Field(type = FieldType.Object)
	private Taxonomy taxonomy;
	@Field(type = FieldType.Nested)
	private ArrayList<Alias> aliases;
	private Svalbard sgsv;
	private boolean inSgsv;
	@Field(type = FieldType.Nested, store = false)
	private ArrayList<Remark> remarks;

	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private ArrayList<String> crops;

	@Field(type = FieldType.Object)
	private Geo geo;
	@Field(type = FieldType.Object)
	private Collect coll;
	private String pedigree;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String donorCode;
	private String donorName;
	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String donorNumb;

	private Date modifiedDate;
	private Date acqusitionDate;

	private boolean historic;

	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String bredCode;

	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private String acceUrl;

	public static AccessionDetails from(AccessionData accession) {
		AccessionDetails ad = new AccessionDetails();
		ad.version = accession.getVersion();
		ad.id = accession.getId();
		ad.uuid = accession.getUuid();
		ad.createdDate = accession.getCreatedDate();
		ad.modifiedDate = accession.getLastModifiedDate();
		ad.acceNumb = accession.getAccessionName();
		ad.acqDate = accession.getAcquisitionDate();
		ad.acqSrc = accession.getAcquisitionSource();
		ad.available = accession.getAvailability();
		ad.historic = accession.getHistoric();
		ad.orgCty = Country.from(accession.getCountryOfOrigin());
		ad.duplSite = MCPDUtil.toStrings(accession.getDuplSite());
		ad.institute = Institute.from(accession.getInstitute());
		ad.mlsStatus = accession.getMlsStatus();
		ad.art15 = accession.getInTrust();
		ad.sampStat = accession.getSampleStatus();
		ad.storage = new ArrayList<Integer>(accession.getStoRage());
		ad.taxonomy = Taxonomy.from(accession.getTaxonomy());
		ad.acceUrl = accession.getAcceUrl();

		if (ad.taxonomy == null) {
			LOG.warn("Go check  " + ad.id);
		}

		return ad;
	}

	public void aliases(List<AccessionAlias> listAccessionAliases) {
		if (listAccessionAliases == null || listAccessionAliases.isEmpty())
			return;
		this.aliases = new ArrayList<Alias>();
		for (AccessionAlias aa : listAccessionAliases) {
			this.aliases.add(Alias.from(aa));
		}
	}

	public void svalbard(SvalbardData svalbardData) {
		this.sgsv = Svalbard.from(svalbardData);
		this.inSgsv = this.sgsv != null;
	}

	public void remarks(List<AccessionRemark> listAccessionRemarks) {
		if (listAccessionRemarks == null || listAccessionRemarks.isEmpty())
			return;
		this.remarks = new ArrayList<Remark>();
		for (AccessionRemark ar : listAccessionRemarks) {
			this.remarks.add(Remark.from(ar));
		}
	}

	public void crops(List<Crop> crops) {
		if (crops == null || crops.isEmpty())
			return;
		this.crops = new ArrayList<String>();
		for (Crop c : crops) {
			this.crops.add(c.getShortName());
		}
	}

	public void geo(AccessionGeo ag) {
		if (ag == null)
			return;
		this.geo = Geo.from(ag);
	}

	public void collect(AccessionCollect collect) {
		this.coll = Collect.from(collect);
	}

	public void breeding(AccessionBreeding breeding) {
		if (breeding == null)
			return;
		this.pedigree = StringUtils.defaultIfBlank(breeding.getPedigree(), null);
		this.bredCode = StringUtils.defaultIfBlank(breeding.getBreederCode(), null);
	}

	public void exch(AccessionExchange exch) {
		if (exch == null)
			return;
		this.donorCode = StringUtils.defaultIfBlank(exch.getDonorInstitute(), null);
		this.donorName = StringUtils.defaultIfBlank(exch.getDonorName(), null);
		this.donorNumb = StringUtils.defaultIfBlank(exch.getAccNumbDonor(), null);
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getAcceNumb() {
		return acceNumb;
	}

	public void setAcceNumb(String acceNumb) {
		this.acceNumb = acceNumb;
	}

	public String getAcqDate() {
		return acqDate;
	}

	public void setAcqDate(String acqDate) {
		this.acqDate = acqDate;
	}

	public String getAcqSrc() {
		return acqSrc;
	}

	public void setAcqSrc(String acqSrc) {
		this.acqSrc = acqSrc;
	}

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public List<String> getDuplSite() {
		return duplSite;
	}

	public void setDuplSite(List<String> duplSite) {
		this.duplSite = duplSite;
	}

	public Boolean getMlsStatus() {
		return mlsStatus;
	}

	public void setMlsStatus(Boolean mlsStatus) {
		this.mlsStatus = mlsStatus;
	}

	public Integer getSampStat() {
		return sampStat;
	}

	public void setSampStat(Integer sampStat) {
		this.sampStat = sampStat;
	}

	public List<Integer> getStorage() {
		return storage;
	}

	public void setStorage(List<Integer> storage) {
		this.storage = storage;
	}

	public Institute getInstitute() {
		return institute;
	}

	public void setInstitute(Institute institute) {
		this.institute = institute;
	}

	public Country getOrgCty() {
		return orgCty;
	}

	public void setOrgCty(Country orgCty) {
		this.orgCty = orgCty;
	}

	public Taxonomy getTaxonomy() {
		return taxonomy;
	}

	public void setTaxonomy(Taxonomy taxonomy) {
		this.taxonomy = taxonomy;
	}

	public ArrayList<Alias> getAliases() {
		return aliases;
	}

	public void setAliases(ArrayList<Alias> aliases) {
		this.aliases = aliases;
	}

	public Svalbard getSgsv() {
		return sgsv;
	}

	public void setSgsv(Svalbard sgsv) {
		this.sgsv = sgsv;
	}

	public ArrayList<Remark> getRemarks() {
		return remarks;
	}

	public void setRemarks(ArrayList<Remark> remarks) {
		this.remarks = remarks;
	}

	public ArrayList<String> getCrops() {
		return crops;
	}

	public void setCrops(ArrayList<String> crops) {
		this.crops = crops;
	}

	public Geo getGeo() {
		return geo;
	}

	public void setGeo(Geo geo) {
		this.geo = geo;
	}

	public Collect getColl() {
		return coll;
	}

	public void setColl(Collect coll) {
		this.coll = coll;
	}

	public String getBredCode() {
		return bredCode;
	}

	public void setBredCode(String bredCode) {
		this.bredCode = bredCode;
	}

	public String getPedigree() {
		return pedigree;
	}

	public void setPedigree(String pedigree) {
		this.pedigree = pedigree;
	}

	public String getDonorCode() {
		return donorCode;
	}

	public void setDonorCode(String donorCode) {
		this.donorCode = donorCode;
	}

	public String getDonorName() {
		return donorName;
	}

	public void setDonorName(String donorName) {
		this.donorName = donorName;
	}

	public String getDonorNumb() {
		return donorNumb;
	}

	public void setDonorNumb(String donorNumb) {
		this.donorNumb = donorNumb;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Date getAcqusitionDate() {
		return acqusitionDate;
	}

	public void setAcqusitionDate(Date acqusitionDate) {
		this.acqusitionDate = acqusitionDate;
	}

	public Boolean getArt15() {
		return art15;
	}

	public void setArt15(Boolean art15) {
		this.art15 = art15;
	}

	public boolean isInSgsv() {
		return inSgsv;
	}

	public void setInSgsv(boolean inSgsv) {
		this.inSgsv = inSgsv;
	}

	public boolean getHistoric() {
		return historic;
	}

	public void setHistoric(boolean historic) {
		this.historic = historic;
	}

	public void networks(List<Organization> organizations) {
		if (organizations == null || organizations.isEmpty())
			return;

		Set<String> networks = new HashSet<String>();
		for (Organization org : organizations) {
			networks.add(org.getSlug());
		}

		if (institute == null)
			institute = new Institute();

		institute.setNetworks(networks);
	}

	public String getAcceUrl() {
		return this.acceUrl;
	}

	public void setAcceUrl(String acceUrl) {
		this.acceUrl = acceUrl;
	}
}
