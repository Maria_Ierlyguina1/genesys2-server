package org.genesys2.server.model.elastic;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.genesys2.server.model.genesys.ExperimentTrait;
import org.genesys2.server.model.genesys.Method;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Parent;

@Document(indexName = "genesys", type = "trait", refreshInterval = "-1")
public class AccessionEsTrait {
	private static final Logger LOG = Logger.getLogger(AccessionDetails.class);

	@Id
	private Long id;
	@Version
	private Long version;

	@Parent(type = "mcpd")
	private long accessionId;

	@Field(index = FieldIndex.not_analyzed, type = FieldType.Long)
	private long methodId;

	@Field(index = FieldIndex.not_analyzed, type = FieldType.String)
	private Set<String> sval = new HashSet<String>();
	@Field(index = FieldIndex.not_analyzed, type = FieldType.Long)
	private Set<Long> ival = new HashSet<Long>();
	@Field(index = FieldIndex.not_analyzed, type = FieldType.Double)
	private Set<Double> dval = new HashSet<Double>();

	public static List<AccessionEsTrait> traits(List<Method> listMethods, Map<Long, List<ExperimentTrait>> accessionTraitValues) {
		ArrayList<AccessionEsTrait> accessionEsTraits = new ArrayList<AccessionEsTrait>();

		if (listMethods == null || listMethods.isEmpty())
			return accessionEsTraits;
		if (accessionTraitValues == null || accessionTraitValues.isEmpty())
			return accessionEsTraits;

		for (Method m : listMethods) {
			List<ExperimentTrait> vals = accessionTraitValues.get(m.getId());
			if (vals != null && !vals.isEmpty())
				accessionEsTraits.add(AccessionEsTrait.from(m, vals));
		}
		return accessionEsTraits;
	}

	public static AccessionEsTrait from(Method m, List<ExperimentTrait> vals) {
		AccessionEsTrait t = new AccessionEsTrait();
		t.methodId = m.getId();

		t.sval = new HashSet<String>();
		for (ExperimentTrait et : vals) {
			Object value = et.getValue();
			if (et == null || value == null)
				continue;

			if (value instanceof String)
				t.sval.add((String) value);
			else if (value instanceof Integer)
				t.ival.add(((Integer) value).longValue());
			else if (value instanceof Long)
				t.ival.add((Long) value);
			else if (value instanceof Double)
				t.dval.add((Double) value);
			else if (value instanceof Float)
				t.dval.add(((Float) value).doubleValue());
			else if (value instanceof BigDecimal)
				t.dval.add(((BigDecimal) value).doubleValue());
			else
				LOG.warn("Unsupported value type " + value.getClass());
		}

		return t;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public long getAccessionId() {
		return accessionId;
	}

	public void setAccessionId(long accessionId) {
		this.accessionId = accessionId;
	}

	public long getMethodId() {
		return methodId;
	}

	public void setMethodId(long methodId) {
		this.methodId = methodId;
	}

	public Set<String> getSval() {
		return sval;
	}

	public void setSval(Set<String> sval) {
		this.sval = sval;
	}

	public Set<Long> getIval() {
		return ival;
	}

	public void setIval(Set<Long> ival) {
		this.ival = ival;
	}

	public Set<Double> getDval() {
		return dval;
	}

	public void setDval(Set<Double> dval) {
		this.dval = dval;
	}

}