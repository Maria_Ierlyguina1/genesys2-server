/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.text.MessageFormat;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang.StringUtils;
import org.genesys2.server.model.GlobalVersionedAuditedModel;
import org.genesys2.server.model.impl.CropTaxonomy;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Cacheable
@Entity
// Add index on all fields
@Table(name = "taxonomy2", uniqueConstraints = { @UniqueConstraint(columnNames = { "genus", "species", "spAuthor", "subtaxa", "subtAuthor" }) })
@JsonIgnoreProperties(ignoreUnknown = true)
public class Taxonomy2 extends GlobalVersionedAuditedModel {

	private static final long serialVersionUID = 8881324404490162933L;

	@Column(nullable = false, length = 100)
	private String genus;

	@Column(nullable = false, length = 100)
	private String species;

	@Column(nullable = false, length = 100)
	private String spAuthor;

	@Column(nullable = false, length = 100)
	private String subtaxa;

	@Column(nullable = false, length = 100)
	private String subtAuthor;

	@Column(nullable = false)
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String taxonName;

	private Long taxGenus;

	private Long taxSpecies;

	@JsonIgnore
	@OneToMany(cascade = {}, fetch = FetchType.LAZY, mappedBy = "taxonomy")
	private List<CropTaxonomy> cropTaxonomies;

	public Taxonomy2() {
	}

	public Taxonomy2(Taxonomy2 taxonomy) {
		if (taxonomy != null) {
			this.genus = taxonomy.genus;
			this.species = taxonomy.species;
			this.spAuthor = taxonomy.spAuthor;
			this.subtaxa = taxonomy.subtaxa;
			this.subtAuthor = taxonomy.subtAuthor;
		}
	}

	@PrePersist
	private void prePersist() {
		this.taxonName = getTaxonName();
	}

	public String getGenus() {
		return this.genus;
	}

	public void setGenus(final String genus) {
		this.genus = genus;
	}

	public String getSpecies() {
		return this.species;
	}

	public void setSpecies(final String species) {
		this.species = species;
	}

	public String getTaxonName() {
		final StringBuilder sb = new StringBuilder();
		sb.append(genus).append(" ").append(species);
		if (StringUtils.isNotBlank(spAuthor)) {
			sb.append(" ").append(spAuthor);
		}
		if (StringUtils.isNotBlank(subtaxa)) {
			sb.append(" ").append(subtaxa);
		}
		if (StringUtils.isNotBlank(subtAuthor)) {
			sb.append(" ").append(subtAuthor);
		}
		return sb.toString();
	}

	public void setTaxonName(String taxonName) {
		// Ignore what they provide
		this.taxonName = getTaxonName();
	}

	// public List<CropTaxonomy> getCropTaxonomies() {
	// return cropTaxonomies;
	// }
	//
	// public void setCropTaxonomies(List<CropTaxonomy> cropTaxonomies) {
	// this.cropTaxonomies = cropTaxonomies;
	// }

	public String getSpAuthor() {
		return spAuthor;
	}

	public void setSpAuthor(String spAuthor) {
		this.spAuthor = spAuthor;
	}

	public String getSubtaxa() {
		return subtaxa;
	}

	public void setSubtaxa(String subtaxa) {
		this.subtaxa = subtaxa;
	}

	public String getSubtAuthor() {
		return subtAuthor;
	}

	public void setSubtAuthor(String subtAuthor) {
		this.subtAuthor = subtAuthor;
	}

	public boolean sameAs(Taxonomy2 taxonomy) {
		return taxonomy == null ? false : id.equals(taxonomy.getId());
	}

	public Long getTaxGenus() {
		return this.taxGenus;
	}

	public void setTaxGenus(Long taxGenus) {
		this.taxGenus = taxGenus;
	}

	public Long getTaxSpecies() {
		return this.taxSpecies;
	}

	public void setTaxSpecies(Long taxSpecies) {
		this.taxSpecies = taxSpecies;
	}

	public List<CropTaxonomy> getCropTaxonomies() {
		return cropTaxonomies;
	}

	public void setCropTaxonomies(List<CropTaxonomy> cropTaxonomies) {
		this.cropTaxonomies = cropTaxonomies;
	}

	@Override
	public String toString() {
		return MessageFormat.format("Tax id={0} taxonName={1} genus={2} sp={3} spauthor={4} subtaxa={5} subtauthor={6}", id, getTaxonName(), genus, species,
				spAuthor, subtaxa, subtAuthor);
	}

}
