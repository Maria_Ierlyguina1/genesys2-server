/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.math.RandomUtils;
import org.genesys2.server.model.VersionedAuditedModel;
import org.genesys2.server.servlet.controller.rest.serialization.MaterialRequestSerializer;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "request")
@JsonSerialize(using = MaterialRequestSerializer.class)
public class MaterialRequest extends VersionedAuditedModel {
	
	private static final long serialVersionUID = 4552573253693001837L;
	public static final int NOTVALIDATED = 0;
	public static final int VALIDATED = 1;
	public static final int DISPATCHED = 2;

	@Column
	private int state = NOTVALIDATED;

	@Column(length = 36, unique = true)
	private String uuid;

	@Column(length = 200, nullable = false)
	private String email;

	@Column(length = 100000)
	@Lob
	@Type(type = "org.hibernate.type.TextType")
	private String body;

	@Column(length = 32, nullable = true)
	private String pid;

	@OneToMany(cascade = {}, mappedBy = "sourceRequest")
	private List<MaterialSubRequest> subRequests;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastReminderDate;

	@PrePersist
	void prepersist() {
		if (this.uuid == null) {
			this.uuid = UUID.nameUUIDFromBytes(("genesys:request:" + email + ":" + System.currentTimeMillis() + ":" + RandomUtils.nextInt(100)).getBytes())
					.toString();
		}
	}

	public MaterialRequest() {
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return MessageFormat.format("Request uuid={0} email={1} body={2}", uuid, email, body);
	}

	/**
	 * @param pid
	 *            EasySMTA PID
	 */
	public void setPid(String pid) {
		this.pid = pid;
	}

	/**
	 * @return EasySMTA PID
	 */
	public String getPid() {
		return pid;
	}

	public List<MaterialSubRequest> getSubRequests() {
		return this.subRequests;
	}

	public void setSubRequests(List<MaterialSubRequest> subRequests) {
		this.subRequests = subRequests;
	}

	public Date getLastReminderDate() {
		return lastReminderDate;
	}

	public void setLastReminderDate(Date lastReminderDate) {
		this.lastReminderDate = lastReminderDate;
	}
}
