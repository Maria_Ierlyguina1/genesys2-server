/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.genesys;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.genesys2.server.model.IdUUID;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.util.MCPDUtil;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

@MappedSuperclass
public abstract class AccessionData implements IdUUID, Serializable {

	private static final long serialVersionUID = -3428918862058441943L;

	public static final List<AccessionData> EMPTY_LIST = Collections.unmodifiableList(new ArrayList<AccessionData>());

	@Id
	private Long id;

	@Version
	private long version = 0;

	@CreatedBy
	private Long createdBy;

	@CreatedDate
	@Column(name = "createdDate")
	private Date createdDate;

	@LastModifiedBy
	private Long lastModifiedBy;

	@LastModifiedDate
	@Column(name = "lastModifiedDate")
	private Date lastModifiedDate;

	@MapsId
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, optional = false, orphanRemoval = false)
	@JoinColumn(name = "id")
	private AccessionId accessionId;

	@Column(name = "instCode", length = 10, nullable = false)
	private String instituteCode;

	@ManyToOne(cascade = {}, optional = false)
	@JoinColumn(name = "instituteId")
	private FaoInstitute institute;

	@Column(name = "acceNumb", nullable = false, length = 128)
	private String accessionName;

	// @Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
	@ManyToOne(cascade = {}, optional = false)
	@JoinColumn(name = "taxonomyId2")
	private Taxonomy2 taxonomy;

	@Column(name = "acqSrc", length = 3)
	private String acquisitionSource;

	@Column(name = "acqDate", length = 12)
	private String acquisitionDate;

	@Column(name = "orgCty", length = 3)
	private String origin;

	@ManyToOne(cascade = {}, optional = true)
	@JoinColumn(name = "orgCtyId", nullable = true)
	private Country countryOfOrigin;

	@Column(name = "duplSite", length = 64)
	private String duplSite;

	@Column(name = "sampStat", length = 3)
	private Integer sampleStatus;

	@Column(name = "inSGSV")
	private Boolean inSvalbard;

	@Column(name = "inTrust")
	private Boolean inTrust;

	@Column(name = "available")
	private Boolean availability;

	@Column(name = "historic", nullable = false)
	private boolean historic = false;

	@Column(name = "mlsStat")
	private Boolean mlsStatus;

	@Column(name = "taxGenus", nullable = false)
	private long taxGenus;

	@Column(name = "storage", length = 64, nullable = true)
	private String storage;

	@Column(name = "acceurl", length = 300, nullable = true)
	private String acceUrl;

	/**
	 * Update MCPD {@link #storage}
	 */
	@PrePersist
	@PreUpdate
	private void prePersist() {
		if (this.countryOfOrigin != null)
			this.origin = this.countryOfOrigin.getCode3();
		else
			this.origin = null;

		this.instituteCode = getInstitute().getCode();
		this.storage=MCPDUtil.toMcpdArray(getStoRage());
	}

	public FaoInstitute getInstitute() {
		return this.institute;
	}

	public void setInstitute(final FaoInstitute institute) {
		this.institute = institute;
	}

	public String getInstituteCode() {
		return instituteCode;
	}

	protected void setInstituteCode(final String instituteCode) {
		this.instituteCode = instituteCode;
	}

	public String getAccessionName() {
		return this.accessionName;
	}

	public void setAccessionName(final String accessionName) {
		this.accessionName = accessionName;
	}

	public Taxonomy2 getTaxonomy() {
		return this.taxonomy;
	}

	public String getAcquisitionSource() {
		return this.acquisitionSource;
	}

	public void setAcquisitionSource(final String acquisitionSource) {
		this.acquisitionSource = acquisitionSource;
	}

	public String getAcquisitionDate() {
		return this.acquisitionDate;
	}

	public void setAcquisitionDate(final String acquisitionDate) {
		this.acquisitionDate = acquisitionDate;
	}

	public String getOrigin() {
		return this.origin;
	}

	protected void setOrigin(final String origin) {
		this.origin = origin;
	}

	public Country getCountryOfOrigin() {
		return countryOfOrigin;
	}

	public void setCountryOfOrigin(Country countryOfOrigin) {
		this.countryOfOrigin = countryOfOrigin;
	}

	public String getDuplSite() {
		return this.duplSite;
	}

	public void setDuplSite(final String duplSite) {
		this.duplSite = duplSite;
	}

	public Integer getSampleStatus() {
		return this.sampleStatus;
	}

	public void setSampleStatus(final Integer sampleStatus) {
		this.sampleStatus = sampleStatus;
	}

	public Boolean getInSvalbard() {
		return this.inSvalbard;
	}

	public void setInSvalbard(final Boolean inSvalbard) {
		this.inSvalbard = inSvalbard;
	}

	public Boolean getInTrust() {
		return this.inTrust;
	}

	public void setInTrust(final Boolean inTrust) {
		this.inTrust = inTrust;
	}

	public Boolean getAvailability() {
		return this.availability;
	}

	public void setAvailability(final Boolean availability) {
		this.availability = availability;
	}

	public Boolean getMlsStatus() {
		return this.mlsStatus;
	}

	public void setMlsStatus(final Boolean mlsStatus) {
		this.mlsStatus = mlsStatus;
	}

	public long getTaxGenus() {
		return taxGenus;
	}

	protected void setTaxGenus(long taxGenus) {
		this.taxGenus = taxGenus;
	}

	public void setTaxonomy(Taxonomy2 taxonomy2) {
		this.taxonomy = taxonomy2;
	}

	public abstract List<Integer> getStoRage();

	public abstract void setStoRage(List<Integer> stoRage);

	/**
	 * Returns {@link #getStoRage()} as MCPD string
	 * 
	 * @return MCPD array string
	 */
	public String getStorage() {
		return storage;
	}

	/**
	 * Use {@link #setStoRage(List)} instead.
	 */
	protected void setStorage(String storage) {
		this.storage = storage;
	}

	public boolean getHistoric() {
		return historic;
	}

	public void setHistoric(boolean historic) {
		this.historic = historic;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Long getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(Long lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public AccessionId getAccessionId() {
		return accessionId;
	}

	public void setAccessionId(AccessionId accessionId) {
		this.accessionId = accessionId;
	}

	@Override
	@Transient
	public UUID getUuid() {
		return accessionId.getUuid();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return MessageFormat.format("Accession id={0,number,##########} UUID={1}", accessionId.getId(), getUuid());
	}

	public String getAcceUrl() {
		return this.acceUrl;
	}

	public void setAcceUrl(String acceUrl) {
		this.acceUrl = acceUrl;
	}
}
