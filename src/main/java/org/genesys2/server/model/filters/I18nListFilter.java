/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.filters;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.list.UnmodifiableList;

public class I18nListFilter<T> extends BasicFilter {
	private List<ValueName<T>> options = new ArrayList<ValueName<T>>();

	public I18nListFilter(String name, DataType dataType) {
		super(name, dataType, FilterType.I18NLIST);
	}

	public GenesysFilter build(String prefix, @SuppressWarnings("unchecked") T... options) {
		final List<ValueName<T>> opts = new ArrayList<ValueName<T>>();
		for (final T opt : options) {
			opts.add(new ValueName<T>(opt, prefix + "." + opt));
		}
		this.options = new UnmodifiableList<ValueName<T>>(opts);
		return this;
	}

	public List<ValueName<T>> getOptions() {
		return options;
	}
}