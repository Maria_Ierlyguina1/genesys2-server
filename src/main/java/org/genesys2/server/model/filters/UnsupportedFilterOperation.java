package org.genesys2.server.model.filters;

public class UnsupportedFilterOperation extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1994534857469773371L;

	public UnsupportedFilterOperation(String message) {
		super(message);
	}

}
