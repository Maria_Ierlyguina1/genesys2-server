package org.genesys2.server.model.dataset;

import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.genesys2.server.model.BusinessModel;

@Entity
@Table(name = "ds2")
public class DS extends BusinessModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8152480522578736947L;

	@Column(columnDefinition = "binary(16)", unique = true, nullable=false, updatable = false)
	private UUID uuid;

	@OrderBy("index")
	@OneToMany(mappedBy = "dataset")
	private List<DSQualifier> qualifiers;

	@OrderBy("index")
	@OneToMany(mappedBy = "dataset")
	private List<DSDescriptor> descriptors;

	/**
	 * Generate UUID if missing
	 */
	@PrePersist
	protected void prepersist() {
		if (this.uuid == null) {
			this.uuid = UUID.randomUUID();
		}
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public List<DSQualifier> getQualifiers() {
		return qualifiers;
	}

	public void setQualifiers(List<DSQualifier> qualifiers) {
		this.qualifiers = qualifiers;
	}

	public List<DSDescriptor> getDescriptors() {
		return descriptors;
	}

	public void setDescriptors(List<DSDescriptor> descriptors) {
		this.descriptors = descriptors;
	}
}
