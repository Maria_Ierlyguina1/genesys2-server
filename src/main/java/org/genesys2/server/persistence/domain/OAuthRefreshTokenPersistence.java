/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.genesys2.server.model.oauth.OAuthRefreshToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface OAuthRefreshTokenPersistence extends JpaRepository<OAuthRefreshToken, Long> {

	@Query("delete from OAuthRefreshToken where value in (:values)")
	@Modifying
	void deleteByTokenValues(@Param("values") List<String> values);

	OAuthRefreshToken findByValue(String value);

	@Query("delete from OAuthRefreshToken where value = ?1")
	@Modifying
	void deleteByValue(String value);

	Collection<OAuthRefreshToken> findByClientId(String clientId);

	@Query("delete from OAuthRefreshToken where clientId = ?1")
	@Modifying
	void deleteByClientId(String clientId);

	@Modifying
	@Query("delete from OAuthRefreshToken where expiration < ?1")
	int deleteOlderThan(Date date);

}
