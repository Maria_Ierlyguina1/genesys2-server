package org.genesys2.server.persistence.domain.dataset;

import org.genesys2.server.model.dataset.DSDescriptor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DSDescriptorRepository extends JpaRepository<DSDescriptor, Long> {

}
