/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.model.genesys.Taxonomy2;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.FaoInstitute;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AccessionRepository extends JpaRepository<Accession, Long> {

	@Query("select a.id from Accession a")
	public List<Long> listAccessionsIds(Pageable pageable);

	@Query(countQuery = "select count(*) from Accession a where a.institute = ?1")
	Page<Accession> findByInstitute(FaoInstitute institute, Pageable pageable);

	List<Accession> findByInstitute(FaoInstitute institute);

	@Query("select count(a) from Accession a where a.institute = ?1")
	long countByInstitute(FaoInstitute institute);

	@Query(value = "select t.genus, count(a.id) from Accession a inner join a.taxonomy t where a.institute = ?1 group by t.genus order by count(a) desc", countQuery = "select count(distinct a.taxonomy.taxGenus) from Accession a where a.institute = ?1")
	Page<Object[]> statisticsGenusInInstitute(FaoInstitute institute, Pageable pageable);

	@Query(value = "select t.taxSpecies, count(a) from Accession a inner join a.taxonomy t where a.institute = ?1 group by t.taxSpecies order by count(a) desc", countQuery = "select count(distinct a.taxonomy.taxSpecies) from Accession a where a.institute = ?1")
	Page<Object[]> statisticsSpeciesInInstitute(FaoInstitute institute, Pageable pageable);

	@Query("select count(a) from Accession a where a.countryOfOrigin = ?1")
	long countByOrigin(Country country);

	@Query("select count(a) from Accession a where a.institute.country = ?1")
	long countByLocation(Country country);

	@Query("select a from Accession a where a.institute in ( ?1 )")
	Page<Accession> findByInstitute(List<FaoInstitute> institutes, Pageable pageable);

	List<Accession> findByInstituteAndAccessionName(FaoInstitute faoInstitute, String accessionName);

	public List<Accession> findByInstituteAndAccessionName(FaoInstitute institute, List<String> acceNumbs);

	@Query("from Accession a where a.id in ( ?1 )")
	Page<Accession> findById(Collection<Long> accessionIds, Pageable pageable);

	@Query("from Accession a where a.id in ( ?1 )")
	List<Accession> listById(Collection<Long> accessionIds, Sort sort);

	Page<Accession> findByInstituteAndTaxonomy(FaoInstitute institute, Taxonomy2 taxonomy, Pageable pageable);

	Page<Accession> findByTaxonomy(Taxonomy2 taxonomy, Pageable pageable);

	@Query("select a from Accession a where a.taxonomy in ( ?1 )")
	Page<Accession> findByTaxonomy(Collection<Taxonomy2> taxonomies, Pageable pageable);

	@Query("select a from Accession a where a.institute.code = ?1 and a.accessionName = ?2")
	Accession findByInstituteCodeAndAccessionName(String instCode, String accessionName);

    @Query("select a from Accession a where a.accessionName=?2 and (a.institute.code=?1 or a.institute.codeSGSV=?1) and a.taxonomy.genus=?3")
    Accession findOneSGSV(String instCode, String acceNumb, String genus);

	@Query("select a from Accession a where a.institute.code=:instCode and a.accessionName=:acceNumb and a.taxonomy.genus=:genus")
	Accession findOne(@Param("instCode") String holdingInstitute, @Param("acceNumb") String accessionName, @Param("genus") String genus);

	@Query("select count(a.id) from Accession a where a.id in ( ?1 ) and a.historic = false and (a.availability is null or a.availability = true) and a.institute.allowMaterialRequests = true")
	long countAvailableForDistribution(Set<Long> accessionIds);

	@Query("select a.id from Accession a where a.id in ( ?1 ) and a.historic = false and (a.availability is null or a.availability = true) and a.institute.allowMaterialRequests = true")
	Set<Long> filterAvailableForDistribution(Set<Long> accessionIds);

	@Modifying
	@Query("update Accession a set a.inSvalbard = true where a in ?1")
	void setInSvalbard(List<Accession> matching);

	@Query("select distinct a.institute from Accession a where a.id in ( ?1 )")
	List<FaoInstitute> findDistinctInstitutesFor(Set<Long> accessionIds);

	@Query("select distinct a.id from Accession a where a.institute = :institute and a.id in ( :ids )")
	Set<Long> findByInstituteAndIds(@Param("institute") FaoInstitute institute, @Param("ids") Set<Long> accessionIds);

	@Query("select a.id from Accession a where a.taxonomy = ?1")
	public List<Long> listAccessionsIds(Taxonomy2 taxonomy);

	@Modifying
	@Query(nativeQuery=true, value="delete from accession where id = ?1")
	public void deleteActive(long id);

	@Query("select a from Accession a where a.accessionId.uuid = ?1")
	public Accession findOneByUuid(UUID uuid);

	public List<Accession> findByAccessionName(String acceNumb);
}
