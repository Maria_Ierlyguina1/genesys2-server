/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import org.genesys2.server.model.genesys.Method;
import org.genesys2.server.model.impl.Country;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.RowCallbackHandler;

public interface GenesysLowlevelRepository {

	void updateCountryRefs();

	void updateFaoInstituteRefs();

	/**
	 * Relink {@link FaoInstitute} with {@link Country} based on institute code
	 */
	void updateFaoInstituteCountries();

	void ensureMethodTable(Method method);

	void refreshMetadataMethods();

	void listAccessions(AppliedFilters filter, RowCallbackHandler rowCallbackHandler);

	void listAccessionsGeo(AppliedFilters filter, RowCallbackHandler rowCallbackHandler);

	void listAccessionsColl(AppliedFilters filter, RowCallbackHandler rowCallbackHandler);

	void listMetadataAccessions(long id, RowCallbackHandler rowCallbackHandler);

	void listAccessionsAlias(AppliedFilters filter, RowCallbackHandler rowCallbackHandler);

	void listAccessionsAccenames(RowCallbackHandler rowCallbackHandler);

	void listAccessionsAlias(RowCallbackHandler rowCallbackHandler);

	int countAccessions(AppliedFilters filter);

	void listAccessionIds(AppliedFilters filter, Sort sort, RowCallbackHandler rowCallbackHandler);

}
