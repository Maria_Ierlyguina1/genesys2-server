<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="faoInstitutes.page.list.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="faoInstitutes.page.list.title" />
	</h1>

	<div class="main-col-header">
		<a href="<c:url value="/wiews/active" />"><spring:message code="faoInstitutes.viewActiveOnly" /></a>
	</div>
	
	<c:if test="${jsonInstitutes ne null}">
		<div class="row" style="">
		<div class="col-sm-12">
			<div id="map" class="gis-map gis-map-square"></div>
		</div>
		</div>

	
<content tag="javascript">
		<script type="text/javascript">
		jQuery(document).ready(function() {	
			GenesysMaps.map("${pageContext.response.locale.language}", $("#map"), {
				maxZoom: 4,
				center: new GenesysMaps.LatLng(0, 0)
			}, function(el, map) {
				var jsonInstitutes=${jsonInstitutes};
				var heatmapData=[];
				jsonInstitutes.forEach(function(inst) {
					heatmapData.push(new google.maps.LatLng(inst.lat, inst.lng));
				});
				var heatmap = new google.maps.visualization.HeatmapLayer({
					data: heatmapData,
					//dissipating: false,
					//gradient: [ "transparent", "#FF0000"],
					//opacity: 1,
					//radius: 3
				});
				heatmap.setMap(map);
				map.fitBounds(GenesysMaps.boundingBox(jsonInstitutes));
			});
		});
		</script>
</content>
	</c:if>
</body>
</html>