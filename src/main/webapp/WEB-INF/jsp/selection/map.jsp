<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="selection.page.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="selection.page.title" />
	</h1>

	<div class="row" style="">
	<div class="col-sm-12">
		<div id="map" class="gis-map gis-map-square"></div>
	</div>
	</div>

<content tag="javascript">		
		<script type="text/javascript">
		jQuery(document).ready(function() {	
			var map = GenesysMaps.map("${pageContext.response.locale.language}", $("#map"), {
			}, function(el, map) {
				$.ajax('<c:url value="/sel/json/geo" />', {
					type : 'get',
					dataType : 'json',
					success : function(respObject) {
						var bounds=new GenesysMaps.BoundingBox();
						respObject.forEach(function(acn) {
							if (acn.lat!=null && acn.lng!=null) {
								var marker = L.marker([acn.lat, acn.lng]).addTo(map);
								marker.bindPopup("<a href='<c:url value="/acn/id/" />" + acn.id +"'>"+acn.accessionName+"</a>");
								bounds.add([acn.lat, acn.lng]);
							}
						});
						map.fitBounds(bounds.getBounds());
					},
					error: function(jqXHR, textStatus, errorThrown) {
						console.log(textStatus);
						console.log(errorThrown);
					}
				});	
			});
		});
		</script>
</content>
</body>
</html>