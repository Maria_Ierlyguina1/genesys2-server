<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html>
<head>
<title><spring:message code="admin.kpi.execution.page" /></title>
</head>
<body>
  <h1>
    <spring:message code="admin.kpi.executionrun.page" />
  </h1>

  <%@ include file="/WEB-INF/jsp/admin/menu.jsp"%>

  <h2>
    <local:prettyTime date="${executionRun.timestamp}" locale="${pageContext.response.locale}" />
  </h2>

  <div class="crop-details">
    <c:forEach items="${observations}" var="obs">
      <div class="row">
        <div class="col-xs-4 col-sm-2 text-right">
          <c:out value="${obs.value}" />
        </div>
        <div class="col-xs-8 col-sm-10">
          <c:forEach items="${obs.dimensions}" var="dim">
            <div>
              <a href="<c:url value="/admin/kpi/exec/${executionRun.execution.name}" />?dk=${dim.id}">
                <c:out value="${dim.value}" />
              </a>
            </div>
          </c:forEach>
        </div>
      </div>
    </c:forEach>
  </div>

</body>
</html>