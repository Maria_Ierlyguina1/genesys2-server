<!DOCTYPE html>

<%@include file="/WEB-INF/jsp/init.jsp"%>

<html>
<head>
<title><spring:message code="userprofile.password" /></title>
</head>
<body>
	<h1>
		<spring:message code="userprofile.password" />
	</h1>
	<c:if test="${error ne null}">
        <div class="alert alert-danger"><spring:message code="verification.invalid-key"/></div>
    </c:if>
    
    <form class="form-horizontal" action="<c:url value="/profile/${tokenUuid}/pwdreset"/>" method="post">
	 	<div class="form-group">
            <label for="password" class="col-lg-2 control-label"><spring:message code="verification.token-key" /></label>
	       <div class="col-lg-3"><input type="text" id="key" name="key" class="span1 form-control" maxlength="4" placeholder="..." value="" /></div>
	    </div>
        <div class="form-group">
            <label for="password" class="col-lg-2 control-label"><spring:message code="userprofile.enter.password" /></label>
            <div class="col-lg-3"><input type="password" id="password" name="password" class="span3 form-control" value="" /></div>
        </div>
        <div class="form-group">
        	<div class="col-lg-1">
                <input type="submit" value="<spring:message code="userprofile.password" />" class="btn btn-primary" />
            </div>
        </div>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
</body>
</html>