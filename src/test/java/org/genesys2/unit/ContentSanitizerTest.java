/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.unit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.model.impl.Article;
import org.junit.Test;
import org.springframework.data.domain.PageRequest;

import java.util.Locale;

import static org.junit.Assert.assertTrue;

public class ContentSanitizerTest extends AbstractServicesTest {

	private static final Log LOG = LogFactory.getLog(DescriptorServiceTest.class);

	@Test
	public void sanitizeAllTest() {
		LOG.info("Start test-method sanitizeAllTest");

		String articleBody = "<script></script><h2><small>Genesys account</small><br />Verify your email address</h2><p>You can already use your Genesys account. We need to confirm your email account before granting you access to all Genesys features.</p><p><a href=\"{0}/profile/{1}/validate\" rel=\"nofollow\">Verify {2}</a></p><h2>Validation key: {3}</h2><p>If you didn&#39;t make this request, <a href=\"{0}/profile/{1}/cancel\" rel=\"nofollow\">click here to cancel</a>.</p><p>Thanks,<br />Genesys team</p>";
		String expectedBody = "<h2><small>Genesys account</small><br />Verify your email address</h2><p>You can already use your Genesys account. We need to confirm your email account before granting you access to all Genesys features.</p><p><a href=\"{0}/profile/{1}/validate\" rel=\"nofollow\">Verify {2}</a></p><h2>Validation key: {3}</h2><p>If you didn&#39;t make this request, <a href=\"{0}/profile/{1}/cancel\" rel=\"nofollow\">click here to cancel</a>.</p><p>Thanks,<br />Genesys team</p>";

		contentService.createGlobalArticle("smtp.email-verification", Locale.ENGLISH, "Test title", articleBody);

		contentSanitizer.sanitizeAll();

		Article articleAfterSanitize = contentService.listArticles(new PageRequest(0, 10)).getContent().get(0);
		assertTrue(!articleAfterSanitize.getBody().equals(articleBody));
		assertTrue(articleAfterSanitize.getBody().equals(expectedBody));

		LOG.info("Test sanitizeAllTest passed!");
	}
}
