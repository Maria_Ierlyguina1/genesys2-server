/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.unit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.UserRole;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.Team;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.spring.SecurityContextUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertTrue;

//import org.mockito.Mockito;

public class TeamServiceTest extends AbstractServicesTest {
	public static final Log LOG = LogFactory.getLog(TeamServiceTest.class);

	private Team team;

	private String teamName;

	private FaoInstitute faoInstitute;

	private User user;

	@Before
	public void setUp() throws Exception {

		teamName = "name of team";

		faoInstitute = new FaoInstitute();
		faoInstitute.setFullName("This is name of institute");
		faoInstitute.setCurrent(true);
		faoInstitute.setPgrActivity(true);
		faoInstitute.setMaintainsCollection(true);
		faoInstitute.setPgrActivity(true);
		faoInstitute.setAccessionCount(1);
		faoInstitute.setUniqueAcceNumbs(true);
		faoInstitute.setCode("Code");

		user = new User();
		user.setName("User's name");
		user.setPassword("User's password");
		user.setEmail("email@test.com");

		team = new Team();
		team.setName(teamName);

		userService.addUser(user);
		List<FaoInstitute> institutes = new ArrayList<>();
		institutes.add(faoInstitute);
		instituteService.update(institutes);

		List<GrantedAuthority> authorities = new ArrayList<>();
		GrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(UserRole.ADMINISTRATOR.getName());
		authorities.add(simpleGrantedAuthority);

		AuthUserDetails authUserDetails = new AuthUserDetails(user.getName(), user.getPassword(), authorities);

		// set actual DB user
		authUserDetails.setUser(userService.getUserById(user.getId()));

		UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(authUserDetails, authUserDetails, authorities);

		SecurityContextHolder.getContext().setAuthentication(authToken);

	}

	@After
	public void teardown() throws UserException {

		teamRepository.deleteAll();
		userService.removeUser(userService.getUserByEmail(user.getEmail()));
		instituteService.delete(faoInstitute.getCode());
		SecurityContextHolder.clearContext();
	}

	@Test
	public void addTeamTets() {
		LOG.info("Start test-method addTeamTest");

		List<Team> firstTeamList = teamService.listTeams(new PageRequest(0, 10)).getContent();
		teamService.addTeam(teamName);
		List<Team> secondTeamList = teamService.listTeams(new PageRequest(0, 10)).getContent();
		assertTrue((secondTeamList.size() - firstTeamList.size()) > 0);

		LOG.info("Test addTeamTest passed");
	}

	@Test
	public void getTeamTest() {
		LOG.info("Start test-method getTeamTest");

		teamService.addTeam(teamName);

		String uuid = teamService.listMyTeams().get(0).getUuid();

		assertTrue(teamService.getTeam(uuid) != null);

		LOG.info("Test getTeamTest is passed");
	}

	@Test
	public void addTeamMemberTest() {
		LOG.info("Start test-method addTeamMemberTest");

		List teamsBeforeAdd = teamService.listTeams(new PageRequest(0, 10)).getContent();

		assertTrue(teamsBeforeAdd.isEmpty());

		teamService.addTeam(teamName);

		List teamsAfterAdd = teamService.listTeams(new PageRequest(0, 10)).getContent();

		assertTrue(!teamsAfterAdd.isEmpty());

		assertTrue(!teamService.getMembers((Team) teamsAfterAdd.get(0)).isEmpty());

		teamService.removeTeamMember(((Team) teamsAfterAdd.get(0)).getUuid(), user);

		assertTrue(teamService.getMembers((Team) teamsAfterAdd.get(0)).isEmpty());

		teamService.addTeamMember(((Team) teamsAfterAdd.get(0)).getUuid(), user);

		assertTrue(!teamService.getMembers((Team) teamsAfterAdd.get(0)).isEmpty());

		LOG.info("Test addTeamMemberTest is passed");
	}

	@Test
	public void removeTeamMemberTest() {
		LOG.info("Start test-method removeTeamMemberTest");

		List teamsBeforeAdd = teamService.listTeams(new PageRequest(0, 10)).getContent();

		assertTrue(teamsBeforeAdd.isEmpty());

		teamService.addTeam(teamName);

		List teamsAfterAdd = teamService.listTeams(new PageRequest(0, 10)).getContent();

		assertTrue(!teamsAfterAdd.isEmpty());

		assertTrue(!teamService.getMembers((Team) teamsAfterAdd.get(0)).isEmpty());

		teamService.removeTeamMember(((Team) teamsAfterAdd.get(0)).getUuid(), user);

		assertTrue(teamService.getMembers((Team) teamsAfterAdd.get(0)).isEmpty());

		LOG.info("Test removeTeamMemberTest is passed");
	}

	@Test
	public void removeMe() {
		LOG.info("Start test-method removeMe");

		List teamsBeforeAdd = teamService.listTeams(new PageRequest(0, 10)).getContent();

		assertTrue(teamsBeforeAdd.isEmpty());

		teamService.addTeam(teamName);

		List teamsAfterAdd = teamService.listTeams(new PageRequest(0, 10)).getContent();

		assertTrue(!teamsAfterAdd.isEmpty());

		assertTrue(!teamService.getMembers((Team) teamsAfterAdd.get(0)).isEmpty());

		final Team teamForRemoveMember = (Team) teamsAfterAdd.get(0);

		teamForRemoveMember.setMembers(new HashSet<User>());

		teamForRemoveMember.getMembers().add(SecurityContextUtil.getCurrentUser());

		teamService.removeMe(teamForRemoveMember);

		assertTrue(teamService.getMembers((Team) teamsAfterAdd.get(0)).isEmpty());

		LOG.info("Test removeMe is passed");
	}

	@Test
	public void addTeamInstituteTest() {
		LOG.info("Start test-method addTeamInstituteTest");

		teamService.addTeam(teamName);

		Team teamForTest = teamService.listTeams(new PageRequest(0, 10)).getContent().get(0);

		teamForTest.setInstitutes(new ArrayList<FaoInstitute>());

		assertTrue(teamForTest.getInstitutes().isEmpty());

		teamService.addTeamInstitute(teamForTest, instituteService.list(new PageRequest(0, 10)).getContent().get(0));

		assertTrue(!teamForTest.getInstitutes().isEmpty());

		LOG.info("Test addTeamInstituteTest is passed");
	}

	@Test
	public void removeTeamInsituteTest() {
		LOG.info("Start test-method removeTeamInsituteTest");

		teamService.addTeam(teamName);

		Team teamForTest = teamService.listTeams(new PageRequest(0, 10)).getContent().get(0);

		teamForTest.setInstitutes(new ArrayList<FaoInstitute>());

		FaoInstitute instituteForTest = instituteService.list(new PageRequest(0, 10)).getContent().get(0);

		teamForTest.getInstitutes().add(instituteForTest);

		assertTrue(!teamForTest.getInstitutes().isEmpty());

		teamService.removeTeamInsitute(teamForTest, instituteForTest);

		assertTrue(teamForTest.getInstitutes().isEmpty());

		LOG.info("Test removeTeamInsituteTest is passed");
	}

	@Test
	public void listUserTeamsTest() {
		LOG.info("Start test-method listUserTeamsTest");

		teamService.addTeam(teamName);

		User userForTest = userService.listUsers(new PageRequest(0, 10)).getContent().get(0);

		List<Team> teamList = teamService.listUserTeams(userForTest);

		assertTrue(!teamList.isEmpty());

		LOG.info("Test listUserTeamsTest is passed");
	}

	@Test
	public void listTeamsTest() {
		LOG.info("Start test-method listTeamsTest");

		teamService.addTeam(teamName);

		List<Team> teamList = teamService.listTeams(new PageRequest(0, 10)).getContent();

		assertTrue(!teamList.isEmpty());

		LOG.info("Test listTeamsTest is passed");
	}

	@Test
	public void getMembersTest() {
		LOG.info("Start test-method getMembersTest");

		teamService.addTeam(teamName);

		List<Team> teamList = teamService.listTeams(new PageRequest(0, 10)).getContent();

		List<User> users = teamService.getMembers(teamList.get(0));

		assertTrue(!users.isEmpty());

		LOG.info("Test getMembersTest is passed");
	}

	@Test
	public void updateTeamInformationTest() {
		LOG.info("Start test-method updateTeamInformationTest");

		teamService.addTeam(teamName);

		Team teamForTest = teamService.listTeams(new PageRequest(0, 10)).getContent().get(0);

		assertTrue(teamForTest.getName().equals(teamName));

		teamService.updateTeamInformation(teamForTest.getUuid(), "new Name new Name");

		Team teamForTest2 = teamService.listTeams(new PageRequest(0, 10)).getContent().get(0);

		assertTrue(teamForTest2.getName().equals("new Name new Name"));

		LOG.info("Test updateTeamInformationTest is passed");
	}
}
