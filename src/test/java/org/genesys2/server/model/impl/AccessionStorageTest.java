/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.genesys2.server.model.genesys.Accession;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.test.BatchRESTServiceTest;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.genesys2.util.MCPDUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = BatchRESTServiceTest.Config.class, initializers = PropertyPlacholderInitializer.class)
@ActiveProfiles("dev")
public class AccessionStorageTest {

	@Autowired
	private GenesysService genesysService;

	@Test
	public void nullStorage() {
		Accession a = new Accession();
		a.setAccessionName("A-1");
		genesysService.saveAccession(a);
	}

	@Test
	public void blankStorage() {
		Accession a = new Accession();
		a.setAccessionName("A-" + System.currentTimeMillis());
		List<Integer> stoRage = new ArrayList<Integer>();
		a.setStoRage(stoRage);
		genesysService.saveAccession(a);

		Long accessionId = a.getId();
		Accession b = genesysService.getAccession(accessionId);
		assertTrue(b.getStorage() == null);
		assertTrue(b.getStoRage() != null);
		assertThat("Accession#stoRage size is not 0", b.getStoRage().size(), is(0));
	}

	@Test
	public void oneStorage() {
		Accession a = new Accession();
		a.setAccessionName("A-" + System.currentTimeMillis());
		List<Integer> stoRage = new ArrayList<Integer>();
		stoRage.add(1);
		a.setStoRage(stoRage);
		genesysService.saveAccession(a);

		Long accessionId = a.getId();
		Accession b = genesysService.getAccession(accessionId);
		assertTrue(b.getStorage() != null);
		assertThat("Storage value should be '1'", a.getStorage(), is("1"));
		assertTrue(b.getStoRage() != null);
	}

	@Test
	public void twoStorage() {
		Accession a = new Accession();
		a.setAccessionName("A-" + System.currentTimeMillis());
		List<Integer> stoRage = new ArrayList<Integer>();
		stoRage.add(1);
		stoRage.add(2);
		a.setStoRage(stoRage);
		genesysService.saveAccession(a);

		Long accessionId = a.getId();
		Accession b = genesysService.getAccession(accessionId);
		assertTrue(b.getStorage() != null);
		assertThat("Storage value should be '1'", a.getStorage(), is("1;2"));
		assertTrue(b.getStoRage() != null);
	}

	@Test
	public void clearStorage() {
		Accession a = new Accession();
		a.setAccessionName("A-" + System.currentTimeMillis());
		List<Integer> stoRage = new ArrayList<Integer>();
		stoRage.add(1);
		stoRage.add(2);
		a.setStoRage(stoRage);
		genesysService.saveAccession(a);

		Long accessionId = a.getId();
		assertNotNull("Accession#id must not be null", a.getId());
		Accession b = genesysService.getAccession(accessionId);
		assertTrue(b.getStorage() != null);
		assertThat("Storage value should be '1'", a.getStorage(), is("1;2"));
		assertTrue(b.getStoRage() != null);

		b.getStoRage().clear();
		genesysService.saveAccession(b);

		Accession c = genesysService.getAccession(accessionId);
		assertNotNull("Accession#stoRage must not be null", c.getStoRage());
		assertThat("Accession#stoRage size is not 0", c.getStoRage().size(), is(0));
		assertThat("Accession#storage is not null", c.getStorage(), is(nullValue()));
	}

	@Test
	public void updateStorage() {
		Accession a = new Accession();
		a.setAccessionName("A-" + System.currentTimeMillis());
		List<Integer> stoRage = new ArrayList<Integer>();
		stoRage.add(1);
		stoRage.add(2);
		stoRage.add(3);
		a.setStoRage(stoRage);
		System.out.println("Saving 1");
		genesysService.saveAccession(a);
		System.out.println("Saving 2");
		genesysService.saveAccession(a);

		Long accessionId = a.getId();
		assertNotNull("Accession#id must not be null", a.getId());
		Accession b = genesysService.getAccession(accessionId);
		assertNotNull(b.getStorage());
		assertNotNull(b.getStoRage());
		assertThat("StoRage length is not 3", a.getStoRage().size(), is(3));
		assertThat("Storage value should be '1;2;3'", a.getStorage(), is("1;2;3"));

		b.getStoRage().remove(0);
		genesysService.saveAccession(b);

		Accession c = genesysService.getAccession(accessionId);
		assertNotNull("Accession#stoRage must not be null", c.getStoRage());
		assertThat("Accession#stoRage size is not 2", c.getStoRage().size(), is(2));
		assertThat("Accession#storage is not '2;3'", c.getStorage(), is("2;3"));
		assertThat("Accession#stoRage does not generate MCPD style string'", c.getStorage(), is(MCPDUtil.toMcpdArray(c.getStoRage())));
		System.out.println("Saving 3");
		genesysService.saveAccession(c);
	}

}
