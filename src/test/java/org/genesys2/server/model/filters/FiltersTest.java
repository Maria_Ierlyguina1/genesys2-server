package org.genesys2.server.model.filters;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.aspect.AsAdminAspect;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.HtmlSanitizer;
import org.genesys2.server.service.TraitService;
import org.genesys2.server.service.UserService;
import org.genesys2.server.service.impl.AclServiceImpl;
import org.genesys2.server.service.impl.FilterHandler;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.OWASPSanitizer;
import org.genesys2.server.service.impl.TraitServiceImpl;
import org.genesys2.server.service.impl.UserServiceImpl;
import org.genesys2.server.test.JpaDataConfig;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = FiltersTest.Config.class, initializers = PropertyPlacholderInitializer.class)
@ActiveProfiles("dev")
public class FiltersTest {
	public static final Log LOG = LogFactory.getLog(FiltersTest.class);

	@Import({ JpaDataConfig.class })
	@ComponentScan(basePackages = { "org.genesys2.server.persistence.domain" })
	public static class Config {
		@Bean
		public FilterHandler filterHandler() {
			return new FilterHandler();
		}

		@Bean
		public ObjectMapper objectMapper() {
			return new ObjectMapper();
		}

		@Bean
		public TraitService traitService() {
			return new TraitServiceImpl();
		}

		@Bean
		public AclService aclService() {
			return new AclServiceImpl();
		}

		@Bean
		public HtmlSanitizer htmlSanitizer() {
			return new OWASPSanitizer();
		}

		@Bean
		public UserService userService() {
			return new UserServiceImpl();
		}

		@Bean
		public AsAdminAspect asAdminAspect() {
			return new AsAdminAspect();
		}

		@Bean
		public CacheManager cacheManager() {
			return new NoOpCacheManager();
		}
	}

	@Autowired
	private FilterHandler filterHandler;

	@Autowired
	private ObjectMapper objectMapper;

	@Test(expected = NullPointerException.class)
	public void testNull() throws JsonParseException, JsonMappingException, IOException {
		AppliedFilters afs = objectMapper.readValue((String) null, AppliedFilters.class);
		LOG.info(afs);
	}

	@Test(expected = JsonMappingException.class)
	public void testBlank() throws JsonParseException, JsonMappingException, IOException {
		AppliedFilters afs = objectMapper.readValue("", AppliedFilters.class);
		LOG.info(afs);
	}

	@Test
	public void testEmpty() throws JsonParseException, JsonMappingException, IOException {
		AppliedFilters afs = objectMapper.readValue("{}", AppliedFilters.class);
		LOG.info(afs);
	}

	@Test
	public void test1() throws JsonGenerationException, JsonMappingException, IOException {
		AppliedFilters afs = new AppliedFilters();

		for (int i = 0; i < 5; i++) {
			FilterHandler.AppliedFilter af = new FilterHandler.AppliedFilter();
			af.setFilterName("key1");
			af.addFilterValue(new FilterHandler.LiteralValueFilter(1));
			af.addFilterValue(new FilterHandler.MinValueFilter(2));
			af.addFilterValue(new FilterHandler.LiteralValueFilter("Test"));
			af.addFilterValue(new FilterHandler.StartsWithFilter("Te"));
			af.addFilterValue(new FilterHandler.MaxValueFilter(2.1f));
			af.addFilterValue(new FilterHandler.ValueRangeFilter(3.0f, -1));
			af.addFilterValue(null);
			afs.add(af);
		}

		LOG.info(objectMapper.writeValueAsString(afs));
	}

	@Test
	public void blankArray() throws JsonParseException, JsonMappingException, IOException {
		AppliedFilters afs = objectMapper.readValue("{\"foo\":[]}", AppliedFilters.class);
		LOG.info(afs);
	}

	@Test
	public void testDeserialize() throws JsonParseException, JsonMappingException, IOException {
		String source = "{\"key\":[1,{\"min\":2},\"Test\"],\"key2\":[{\"max\":2.1},{\"range\":[-1,3.0]}],\"key3\":[{\"like\":\"Te\"},null]}";
		AppliedFilters afs = objectMapper.readValue(source, AppliedFilters.class);

		String result = objectMapper.writeValueAsString(afs);
		LOG.info(result);
		assertTrue(source.equals(result));
	}

	@Test
	public void testLiterals() throws IOException {
		AppliedFilters afs = new AppliedFilters();
		afs.add(new AppliedFilter().setFilterName("key1").addFilterValue(new FilterHandler.LiteralValueFilter(1)).addFilterValue(null));
		afs.add(new AppliedFilter().setFilterName("key2").addFilterValue(new FilterHandler.LiteralValueFilter("Test 1")));

		String source = objectMapper.writeValueAsString(afs);
		LOG.info(source);

		AppliedFilters afs2 = objectMapper.readValue(source, AppliedFilters.class);
		String result = objectMapper.writeValueAsString(afs2);
		LOG.info(result);
		assertTrue(source.equals(result));
	}

	@Test
	public void testRange() throws IOException {
		AppliedFilters afs = new AppliedFilters();
		afs.add(new AppliedFilter().setFilterName("key1").addFilterValue(new FilterHandler.ValueRangeFilter(20, -10)).addFilterValue(null));
		afs.add(new AppliedFilter().setFilterName("key2.two").addFilterValue(new FilterHandler.ValueRangeFilter(-20, 10)));

		String source = objectMapper.writeValueAsString(afs);
		LOG.info(source);

		AppliedFilters afs2 = objectMapper.readValue(source, AppliedFilters.class);
		String result = objectMapper.writeValueAsString(afs2);
		LOG.info(result);
		assertTrue(source.equals(result));
	}

	@Test
	public void testMax() throws IOException {
		AppliedFilters afs = new AppliedFilters();
		afs.add(new AppliedFilter().setFilterName("key1").addFilterValue(new FilterHandler.MaxValueFilter(20.2)).addFilterValue(null));
		afs.add(new AppliedFilter().setFilterName("key2.two").addFilterValue(new FilterHandler.MaxValueFilter(-10.2f)));

		String source = objectMapper.writeValueAsString(afs);
		LOG.info(source);

		AppliedFilters afs2 = objectMapper.readValue(source, AppliedFilters.class);
		String result = objectMapper.writeValueAsString(afs2);
		LOG.info(result);
		assertTrue(source.equals(result));
	}

	@Test
	public void testMin() throws IOException {
		AppliedFilters afs = new AppliedFilters();
		afs.add(new AppliedFilter().setFilterName("key1").addFilterValue(new FilterHandler.MinValueFilter(20.2)).addFilterValue(null));
		afs.add(new AppliedFilter().setFilterName("key2.two").addFilterValue(new FilterHandler.MinValueFilter(-10.2f)));

		String source = objectMapper.writeValueAsString(afs);
		LOG.info(source);

		AppliedFilters afs2 = objectMapper.readValue(source, AppliedFilters.class);
		String result = objectMapper.writeValueAsString(afs2);
		LOG.info(result);
		assertTrue(source.equals(result));
	}

	@Test
	public void testStartsWith() throws IOException {
		AppliedFilters afs = new AppliedFilters();
		afs.add(new AppliedFilter().setFilterName("key1").addFilterValue(new FilterHandler.StartsWithFilter("Tes")).addFilterValue(null));
		afs.add(new AppliedFilter().setFilterName("key2.two").addFilterValue(new FilterHandler.StartsWithFilter("Haha")));

		String source = objectMapper.writeValueAsString(afs);
		LOG.info(source);

		AppliedFilters afs2 = objectMapper.readValue(source, AppliedFilters.class);
		String result = objectMapper.writeValueAsString(afs2);
		LOG.info(result);
		assertTrue(source.equals(result));
	}

	@Test
	public void testMix() throws IOException {
		AppliedFilters afs = new AppliedFilters();
		afs.add(new AppliedFilter().setFilterName("key1").addFilterValue(new FilterHandler.MaxValueFilter(20.2))
				.addFilterValue(new FilterHandler.StartsWithFilter("Tes")).addFilterValue(null));
		afs.add(new AppliedFilter().setFilterName("key2.two").addFilterValue(new FilterHandler.StartsWithFilter("Haha"))
				.addFilterValue(new FilterHandler.MinValueFilter(20.2)).addFilterValue(new FilterHandler.ValueRangeFilter(20, -10)));

		String source = objectMapper.writeValueAsString(afs);
		LOG.info(source);

		AppliedFilters afs2 = objectMapper.readValue(source, AppliedFilters.class);
		String result = objectMapper.writeValueAsString(afs2);
		LOG.info(result);

		assertTrue(afs2.equals(afs));
	}
}
