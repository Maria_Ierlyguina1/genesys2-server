/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import static org.genesys2.server.service.MappingService.CoordUtil.latToImg;
import static org.genesys2.server.service.MappingService.CoordUtil.lonToImg;
import static org.genesys2.server.service.MappingService.CoordUtil.tileToLat;
import static org.genesys2.server.service.MappingService.CoordUtil.tileToLon;
import static org.junit.Assert.assertEquals;

import org.genesys2.server.service.MappingService.CoordUtil;
import org.junit.Test;

public class TilesMathTest {

	@Test
	public void test1() {
		assertEquals(-180.0, tileToLon(1, 0), 0);
		assertEquals(0.0, tileToLon(1, 1), 0);
		assertEquals(180.0, tileToLon(1, 2), 0);

		assertEquals(85.0, tileToLat(1, 0), 0.1);
		assertEquals(0.0, tileToLat(1, 1), 0.1);
		assertEquals(-85, tileToLat(1, 2), 0.1);

		assertEquals(0, tileToLon(2, 2), 0);
		assertEquals(90, tileToLon(2, 3), 0);
		assertEquals(180, tileToLon(2, 4), 0);
	}

	@Test
	public void testLonToImg() {
		assertEquals(0, lonToImg(1, -180.0));
		assertEquals(0, lonToImg(2, -180.0));
		assertEquals(0, lonToImg(3, -180.0));
		assertEquals(0, lonToImg(4, -180.0));
		assertEquals(0, lonToImg(5, -180.0));

		assertEquals(255, lonToImg(1, 179.99999));
		assertEquals(255, lonToImg(2, 179.99999));
		assertEquals(255, lonToImg(3, 179.99999));
		assertEquals(255, lonToImg(4, 179.99999));
		assertEquals(255, lonToImg(5, 179.99999));
	}

	@Test
	public void testLatToImg() {
		// assertEquals(0, latToImg(1, 0));
		// assertEquals(0, latToImg(1, 85));
		// assertEquals(0, latToImg(2, 85));
		// assertEquals(0, latToImg(3, 85));
		// assertEquals(0, latToImg(4, 85));
		// assertEquals(0, latToImg(5, 85));
		// assertEquals(0, latToImg(6, 85));

		// System.err.println("HERE" + latToImg(0, 85));
		// System.err.println("HERE" + latToImg(1, 85));
		// System.err.println("HERE" + latToImg(2, 85));

		System.err.println("HERE=" + latToImg(0, 85));
		System.err.println("HERE=" + latToImg(0, -85.010));
		System.err.println("......");
		System.err.println("HERE=" + latToImg(1, 85));
		System.err.println("HERE=" + latToImg(1, -85.01));
	}

	@Test
	public void test2LatToImg() {
		System.err.println(CoordUtil.tileToLon1(1, 1));
		System.err.println(CoordUtil.tileToLon(1, 1));
	}
}
