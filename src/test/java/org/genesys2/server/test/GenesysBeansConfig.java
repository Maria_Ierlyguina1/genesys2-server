/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import org.genesys2.server.aspect.AsAdminAspect;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.UserService;
import org.genesys2.server.service.audit.SpringSecurityAuditorAware;
import org.genesys2.server.service.impl.AclServiceImpl;
import org.genesys2.server.service.impl.GenesysServiceImpl;
import org.genesys2.server.service.impl.GeoServiceImpl;
import org.genesys2.server.service.impl.TaxonomyServiceImpl;
import org.genesys2.server.service.impl.UserServiceImpl;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.AuditorAware;

@ComponentScan(basePackages = {"org.genesys2.server.persistence.domain"})
public class GenesysBeansConfig extends JpaDataConfig {
    @Bean
    public AsAdminAspect asAdminAspect() {
        return new AsAdminAspect();
    }

    @Bean
    public AuditorAware<?> auditorAware() {
        return new SpringSecurityAuditorAware();
    }

    @Bean
    public GeoService geoService() {
        return new GeoServiceImpl();
    }

    @Bean
    public GenesysService genesysService() {
        return new GenesysServiceImpl();
    }

    @Bean
    public TaxonomyService taxonomyService() {
        return new TaxonomyServiceImpl();
    }

    @Bean
    public UserService userService() {
        return new UserServiceImpl();
    }

    @Bean
    public AclService aclService() {
        return new AclServiceImpl();
    }

    @Bean
    public CacheManager cacheManager() {
        return new NoOpCacheManager();
    }

}
