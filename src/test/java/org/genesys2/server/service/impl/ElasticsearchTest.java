/**
 * Copyright 2014 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.service.impl;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.genesys2.server.aspect.AsAdminAspect;
import org.genesys2.server.model.elastic.AccessionDetails;
import org.genesys2.server.model.impl.FaoInstitute;
import org.genesys2.server.model.impl.Organization;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.ElasticService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysFilterService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.HtmlSanitizer;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.TraitService;
import org.genesys2.server.service.UserService;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.test.JpaDataConfig;
import org.genesys2.server.test.PropertyPlacholderInitializer;
import org.genesys2.spring.config.ElasticsearchConfig;
import org.genesys2.spring.config.HazelcastConfig;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.facet.result.Term;
import org.springframework.data.elasticsearch.core.facet.result.TermResult;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ElasticsearchTest.Config.class, initializers = PropertyPlacholderInitializer.class)
@ActiveProfiles("cacher")
public class ElasticsearchTest {
	private static final Logger LOG = Logger.getLogger(ElasticsearchTest.class);

	@Import({ ElasticsearchConfig.class, JpaDataConfig.class, HazelcastConfig.class })
	@ComponentScan(basePackages = { "org.genesys2.server.persistence.domain" })
	public static class Config {

		@Bean
		public FilterHandler filterHadler() {
			return new FilterHandler();
		}

		@Bean
		public ElasticService searchService() {
			return new ElasticsearchSearchServiceImpl();
		}

		@Bean
		public ObjectMapper objectMapper() {
			return new ObjectMapper();
		}

		@Bean
		public AsAdminAspect asAdminAspect() {
			return new AsAdminAspect();
		}

		@Bean
		public UserService userService() {
			return new UserServiceImpl();
		}

		@Bean
		public AclService aclService() {
			return new AclServiceImpl();
		}

		@Bean
		public TaxonomyService taxonomyService() {
			return new TaxonomyServiceImpl();
		}

		@Bean
		public GenesysService genesysService() {
			return new GenesysServiceImpl();
		}

		@Bean
		public CacheManager cacheManager() {
			return new NoOpCacheManager();
		}

		@Bean
		public HtmlSanitizer htmlSanitizer() {
			return new OWASPSanitizer();
		}

		@Bean
		public GeoService geoService() {
			return new GeoServiceImpl();
		}

		@Bean
		public ContentService contentService() {
			return new ContentServiceImpl();
		}

		@Bean
		public VelocityEngine velocityEngine() throws VelocityException, IOException {
			final VelocityEngineFactoryBean vf = new VelocityEngineFactoryBean();
			return vf.createVelocityEngine();
		}

		@Bean
		public OrganizationService organizationService() {
			return new OrganizationServiceImpl();
		}

		@Bean
		public InstituteService instituteService() {
			return new InstituteServiceImpl();
		}

		@Bean
		public CropService cropService() {
			return new CropServiceImpl();
		}

		@Bean
		public GenesysFilterService genesysFilterService() {
			return new GenesysFilterServiceImpl();
		}

		@Bean
		private TraitService traitService() {
			return new TraitServiceImpl();
		}
	}

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private ElasticService elasticService;

	@Autowired
	private OrganizationService organizationService;

	@Test
	public void testzz() throws JsonProcessingException, SearchException {
		Page<AccessionDetails> a = elasticService.search("+acceNumb='PI 584950' +orgCty.iso3='THA' +taxonomy.genus=Vigna", new PageRequest(0, 10));
		LOG.info("Size: " + a.getNumberOfElements() + " total=" + a.getTotalElements());
		for (AccessionDetails aa : a.getContent()) {
			LOG.info(mapper.writeValueAsString(aa));
		}
	}

	@Test
	public void testInstituteCode() throws JsonProcessingException, SearchException {
		AppliedFilters filters = new AppliedFilters();
		filters.add(new AppliedFilter().setFilterName(FilterConstants.INSTCODE).addFilterValue(new FilterHandler.LiteralValueFilter("NGA039")));

		LOG.info(filters);
		Page<AccessionDetails> a = elasticService.filter(filters, new PageRequest(0, 100));
		LOG.info("Size: " + a.getNumberOfElements() + " total=" + a.getTotalElements());
		for (AccessionDetails aa : a.getContent()) {
			assertTrue("NGA039".equals(aa.getInstitute().getCode()));
			LOG.info(mapper.writeValueAsString(aa));
		}
	}

	@Test
	public void testGenus() throws JsonProcessingException, SearchException {
		AppliedFilters filters = new AppliedFilters();
		filters.add(new AppliedFilter().setFilterName(FilterConstants.TAXONOMY_GENUS).addFilterValue(new FilterHandler.LiteralValueFilter("Vigna")));

		LOG.info(filters);
		Page<AccessionDetails> a = elasticService.filter(filters, new PageRequest(0, 100));
		LOG.info("Size: " + a.getNumberOfElements() + " total=" + a.getTotalElements());
		for (AccessionDetails aa : a.getContent()) {
			assertTrue("Vigna".equals(aa.getTaxonomy().getGenus()));
			LOG.info(mapper.writeValueAsString(aa));
		}

	}

	@Test
	public void testGenusAndSpecies() throws JsonProcessingException, SearchException {
		AppliedFilters filters = new AppliedFilters();
		filters.add(new AppliedFilter().setFilterName(FilterConstants.TAXONOMY_GENUS).addFilterValue(new FilterHandler.LiteralValueFilter("Vigna")));
		filters.add(new AppliedFilter().setFilterName(FilterConstants.TAXONOMY_SPECIES).addFilterValue(new FilterHandler.LiteralValueFilter("unguiculata")));

		LOG.info(filters);
		Page<AccessionDetails> a = elasticService.filter(filters, new PageRequest(0, 100));
		LOG.info("Size: " + a.getNumberOfElements() + " total=" + a.getTotalElements());
		for (AccessionDetails aa : a.getContent()) {
			assertTrue("Vigna".equals(aa.getTaxonomy().getGenus()));
			assertTrue("unguiculata".equals(aa.getTaxonomy().getSpecies()));
			LOG.info(mapper.writeValueAsString(aa));
		}

	}

	@Test
	public void testAcceNumb() throws JsonProcessingException, SearchException {
		AppliedFilters filters = new AppliedFilters();
		filters.add(new AppliedFilter().setFilterName(FilterConstants.ACCENUMB).addFilterValue(new FilterHandler.LiteralValueFilter("TVu-7268")));

		LOG.info(filters);
		Page<AccessionDetails> a = elasticService.filter(filters, new PageRequest(0, 100));
		LOG.info("Size: " + a.getNumberOfElements() + " total=" + a.getTotalElements());
		for (AccessionDetails aa : a.getContent()) {
			assertTrue("TVu-7268".equals(aa.getAcceNumb()));
			LOG.info(mapper.writeValueAsString(aa));
		}
	}

	@Test
	public void testAcceNumbStarts() throws JsonProcessingException, SearchException {
		AppliedFilters filters = new AppliedFilters();
		filters.add(new AppliedFilter().setFilterName(FilterConstants.ACCENUMB).addFilterValue(new FilterHandler.StartsWithFilter("ITC05")));

		LOG.info(filters);
		Page<AccessionDetails> a = elasticService.filter(filters, new PageRequest(0, 100));
		LOG.info("Size: " + a.getNumberOfElements() + " total=" + a.getTotalElements());
		for (AccessionDetails aa : a.getContent()) {
			assertTrue(aa.getAcceNumb().startsWith("ITC05"));
			LOG.info(mapper.writeValueAsString(aa));
		}
	}

	@Test
	public void testAccenumb() throws JsonProcessingException, SearchException {
		AppliedFilters filters = new AppliedFilters();
		filters.add(new AppliedFilter().setFilterName(FilterConstants.ACCENUMB).addFilterValue(new FilterHandler.LiteralValueFilter("VI039981"))
				.addFilterValue(new FilterHandler.LiteralValueFilter("VI039982")));

		LOG.info(filters);
		Page<AccessionDetails> a = elasticService.filter(filters, new PageRequest(0, 100));
		LOG.info("Size: " + a.getNumberOfElements() + " total=" + a.getTotalElements());
		for (AccessionDetails aa : a.getContent()) {
			LOG.info(mapper.writeValueAsString(aa));
		}
	}

	@Test
	public void testAliases() throws JsonProcessingException, SearchException {
		AppliedFilters filters = new AppliedFilters();
		filters.add(new AppliedFilter().setFilterName(FilterConstants.ACCENUMB).addFilterValue(new FilterHandler.LiteralValueFilter("VI039981"))
				.addFilterValue(new FilterHandler.LiteralValueFilter("VI039982")));
		filters.add(new AppliedFilter().setFilterName(FilterConstants.ALIAS).addFilterValue(new FilterHandler.LiteralValueFilter("TOT0956")));

		LOG.info(filters);
		Page<AccessionDetails> a = elasticService.filter(filters, new PageRequest(0, 100));
		LOG.info("Size: " + a.getNumberOfElements() + " total=" + a.getTotalElements());
		for (AccessionDetails aa : a.getContent()) {
			LOG.info(mapper.writeValueAsString(aa));
		}
	}

	@Test
	public void testSampStat() throws JsonProcessingException, SearchException {
		AppliedFilters filters = new AppliedFilters();
		filters.add(new AppliedFilter().setFilterName(FilterConstants.SAMPSTAT).addFilterValue(new FilterHandler.LiteralValueFilter(110))
				.addFilterValue(new FilterHandler.LiteralValueFilter(120)));

		LOG.info(filters);
		Page<AccessionDetails> a = elasticService.filter(filters, new PageRequest(0, 100));
		LOG.info("Size: " + a.getNumberOfElements() + " total=" + a.getTotalElements());
		for (AccessionDetails aa : a.getContent()) {
			assertTrue(aa.getSampStat() == 110 || aa.getSampStat() == 120);
			LOG.info(mapper.writeValueAsString(aa));
		}
	}

	@Test
	public void testStorage() throws JsonProcessingException, SearchException {
		AppliedFilters filters = new AppliedFilters();
		filters.add(new AppliedFilter().setFilterName(FilterConstants.STORAGE).addFilterValue(new FilterHandler.LiteralValueFilter(30))
				.addFilterValue(new FilterHandler.LiteralValueFilter(40)));

		LOG.info(filters);
		Page<AccessionDetails> a = elasticService.filter(filters, new PageRequest(0, 100));
		LOG.info("Size: " + a.getNumberOfElements() + " total=" + a.getTotalElements());
		for (AccessionDetails aa : a.getContent()) {
			assertTrue(aa.getStorage().contains(40) || aa.getStorage().contains(30));
			LOG.info(mapper.writeValueAsString(aa));
		}
	}

	@Test
	public void testStorageStats() throws JsonProcessingException, SearchException {
		AppliedFilters filters = new AppliedFilters();
		filters.add(new AppliedFilter().setFilterName(FilterConstants.STORAGE).addFilterValue(new FilterHandler.LiteralValueFilter(30))
				.addFilterValue(new FilterHandler.LiteralValueFilter(40)));

		LOG.info(filters);
		TermResult termResult = elasticService.termStatistics(filters, FilterConstants.STORAGE, 3);
		for (Term t : termResult.getTerms()) {
			LOG.info(t.getTerm() + " = " + t.getCount());
		}
		LOG.info("Other=" + termResult.getOtherCount());
		LOG.info("Missing=" + termResult.getMissingCount());
		LOG.info("Total=" + termResult.getTotalCount());
	}

	@Test
	public void testAvailable() throws JsonProcessingException, SearchException {
		AppliedFilters filters = new AppliedFilters();
		filters.add(new AppliedFilter().setFilterName(FilterConstants.AVAILABLE).addFilterValue(new FilterHandler.LiteralValueFilter(true))
				.addFilterValue(null));

		LOG.info(filters);
		Page<AccessionDetails> a = elasticService.filter(filters, new PageRequest(0, 100));
		LOG.info("Size: " + a.getNumberOfElements() + " total=" + a.getTotalElements());
		for (AccessionDetails aa : a.getContent()) {
			assertTrue(aa.getAvailable() == null || aa.getAvailable() == Boolean.TRUE);
			LOG.info(mapper.writeValueAsString(aa));
		}
	}

	@Test
	public void testCollMissId() throws JsonProcessingException, SearchException {
		AppliedFilters filters = new AppliedFilters();
		filters.add(new AppliedFilter().setFilterName(FilterConstants.COLLMISSID).addFilterValue(
				new FilterHandler.LiteralValueFilter("SDC-CLVNET GERMPLASM COLLECTING PROJECT")));

		LOG.info(filters);
		Page<AccessionDetails> a = elasticService.filter(filters, new PageRequest(0, 100));
		LOG.info("Size: " + a.getNumberOfElements() + " total=" + a.getTotalElements());
		for (AccessionDetails aa : a.getContent()) {
			assertTrue(aa.getColl().collMissId.equals("SDC-CLVNET GERMPLASM COLLECTING PROJECT"));
			LOG.info(mapper.writeValueAsString(aa));
		}
	}

	@Test
	public void testSgsv() throws JsonProcessingException, SearchException {
		AppliedFilters filters = new AppliedFilters();
		filters.add(new AppliedFilter().setFilterName(FilterConstants.SGSV).addFilterValue(new FilterHandler.LiteralValueFilter(true)));

		LOG.info(filters);
		Page<AccessionDetails> a = elasticService.filter(filters, new PageRequest(0, 100));
		LOG.info("Size: " + a.getNumberOfElements() + " total=" + a.getTotalElements());
		for (AccessionDetails aa : a.getContent()) {
			assertTrue(aa.getSgsv() != null);
			LOG.info(mapper.writeValueAsString(aa));
		}

		filters = new AppliedFilters();
		filters.add(new AppliedFilter().setFilterName(FilterConstants.SGSV).addFilterValue(new FilterHandler.LiteralValueFilter(false)));

		LOG.info(filters);
		a = elasticService.filter(filters, new PageRequest(0, 100));
		LOG.info("Size: " + a.getNumberOfElements() + " total=" + a.getTotalElements());
		for (AccessionDetails aa : a.getContent()) {
			assertTrue(aa.getSgsv() == null);
			LOG.info(mapper.writeValueAsString(aa));
		}
	}

	@Test
	public void testGeo() throws JsonProcessingException, IOException, SearchException {
		// {"sgsv":[],"sampStat":[],"geo.latitude":[{"range":[-20,30]}]}
		AppliedFilters filters = mapper
				.readValue("{\"sgsv\":[],\"sampStat\":[],\"geo.latitude\":[{\"range\":[-20,30]},{\"max\":-50.1}]}", AppliedFilters.class);

		LOG.info(filters);
		Page<AccessionDetails> a = elasticService.filter(filters, new PageRequest(0, 100));
		LOG.info("Size: " + a.getNumberOfElements() + " total=" + a.getTotalElements());
		for (AccessionDetails aa : a.getContent()) {
			// LOG.info(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(aa));
			LOG.info(mapper.writeValueAsString(aa));
		}
	}

	@Ignore("We don't have organizations in the hsql db")
	@Test
	public void testOrganization() throws JsonProcessingException, SearchException {
		AppliedFilters filters = new AppliedFilters();
		filters.add(new AppliedFilter().setFilterName(FilterConstants.INSTITUTE_NETWORK).addFilterValue(new FilterHandler.LiteralValueFilter("EURISCO")));

		Set<String> organizationInst = new HashSet<String>();
		Organization org = organizationService.getOrganization("EURISCO");
		if (org != null)
			for (FaoInstitute institue : organizationService.getMembers(org)) {
				organizationInst.add(institue.getCode());
			}

		LOG.info(filters);
		Page<AccessionDetails> a = elasticService.filter(filters, new PageRequest(0, 100));
		LOG.info("Size: " + a.getNumberOfElements() + " total=" + a.getTotalElements());
		for (AccessionDetails aa : a.getContent()) {
			assertTrue(organizationInst.contains(aa.getInstitute().getCode()));
			LOG.info(mapper.writeValueAsString(aa));
		}
	}

}
